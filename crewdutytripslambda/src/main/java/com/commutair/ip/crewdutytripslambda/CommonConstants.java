package com.commutair.ip.crewdutytripslambda;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";
	String ISO_DATE_FORMAT = "yyyy-MM-dd";
	String CALENDAR_KEY = "CrewClndr";
	String TRIP_KEY = "CrewDutyTrip";
	String SPACE = " ";
	String DATE_TIME_FORMAT_1 = "yyyy-MM-dd HHmm";
	String DEFAULT_START_TIME = "0000";
}

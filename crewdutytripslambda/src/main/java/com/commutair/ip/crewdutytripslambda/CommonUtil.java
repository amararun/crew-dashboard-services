package com.commutair.ip.crewdutytripslambda;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CommonUtil {

	/**
	 * This method converts current date in required format
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDate(String format) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	/**
	 * This method converts date from one format to another format
	 * 
	 * @param date
	 * @param fromFormat
	 * @param toFormat
	 * @return mintDate
	 * @throws ParseException
	 */
	public static String getCustomizedDate(String date, String fromFormat, String toFormat) throws ParseException {
		SimpleDateFormat dataPointFormat = new SimpleDateFormat(fromFormat);
		Date dpDate = dataPointFormat.parse(date);
		SimpleDateFormat mintFormat = new SimpleDateFormat(toFormat);
		String mintDate = mintFormat.format(dpDate);
		// mintDate = mintDate + " 00:00 - " + mintDate + " 24:00";
		return mintDate;
	}

	public static Date getDate(String date, String format) throws ParseException {
		SimpleDateFormat dataPointFormat = new SimpleDateFormat(format);
		Date dpDate = dataPointFormat.parse(date);
		return dpDate;
	}

	/**
	 * This method converts given date in required format
	 * 
	 * @param date
	 * @param dateFormat
	 * @return formatedDate
	 */
	public String convertDateToString(Date date, String dateFormat) {
		String formatedDate = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		formatedDate = simpleDateFormat.format(date);
		return formatedDate;
	}

	/**
	 * This method stack trace of given exception
	 * 
	 * @param e
	 * @return
	 */
	public static String getExceptionStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	public static long getTTLUnits(String date, String format, int numberOfDays) throws ParseException {
		// SimpleDateFormat dataPointFormat = new SimpleDateFormat(format);
		// Date dpDate = dataPointFormat.parse(date);
		// mintDate = mintDate + " 00:00 - " + mintDate + " 24:00";
		Calendar calendar = Calendar.getInstance();
		// calendar.setTime(dpDate);
		// calendar.add(Calendar.DAY_OF_MONTH, numberOfDays); //add days
		calendar.add(Calendar.DAY_OF_MONTH, numberOfDays);
		long ttl = (calendar.getTimeInMillis() / 1000L);
		return ttl;
	}

	public static boolean validateDateFormat(String date) {
		boolean flag = false;
		if (null != date && !("".equals(date.trim())) && date.matches("([0-9]{2})\\.([0-9]{2})\\.([0-9]{4})")) {
			return true;
		}
		return flag;
	}

	public static String getCurrentESTTime(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZoneId newYokZoneId = ZoneId.of("America/New_York");
		ZonedDateTime zonedDateTime = dateTime.atZone(newYokZoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(zonedDateTime);
	}

	public static String findBidMonth(String tripDate) throws ParseException {
		// String tripDate=CommonUtil.getCustomizedDate(tripDate, "MM/dd/yyyy",
		// "yyyy-MM-dd");
		Date tripDt = CommonUtil.getDate(tripDate, "yyyy-MM-dd");
		String year = CommonUtil.getCustomizedDate(tripDate, "yyyy-MM-dd", "yyyy");
		if (tripDt.compareTo(CommonUtil.getDate("01/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("01/30/" + year, "MM/dd/yyyy")) <= 0)
			return "01/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("01/31/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("03/01/" + year, "MM/dd/yyyy")) <= 0)
			return "02/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("03/02/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("03/31/" + year, "MM/dd/yyyy")) <= 0)
			return "03/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("04/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("04/30/" + year, "MM/dd/yyyy")) <= 0)
			return "04/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("05/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("05/31/" + year, "MM/dd/yyyy")) <= 0)
			return "05/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("06/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("06/30/" + year, "MM/dd/yyyy")) <= 0)
			return "06/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("07/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("07/31/" + year, "MM/dd/yyyy")) <= 0)
			return "07/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("08/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("08/31/" + year, "MM/dd/yyyy")) <= 0)
			return "08/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("09/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("09/30/" + year, "MM/dd/yyyy")) <= 0)
			return "09/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("10/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("10/31/" + year, "MM/dd/yyyy")) <= 0)
			return "10/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("11/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("11/30/" + year, "MM/dd/yyyy")) <= 0)
			return "11/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("12/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("12/31/" + year, "MM/dd/yyyy")) <= 0)
			return "12/" + year;
		return null;
	}

	public static Response setResponse(String responseBody, int statusCode) {
		Response response = new Response();
		// response.setMessage("Saved Successfully!!!");
		response.setBase64Encoded(false);
		response.setStatusCode(statusCode);
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("service-name", "Crew Duty Trips");
		response.setHeaders(headerMap);
		response.setBody(responseBody);
		return response;
	}

	public static Response handleException(Exception e, CrewDutyTripsDTO crewDutyTripsDTO) {
		Response response = null;
		response = setResponse("{\"message\":\"Crew Data Trip Process Failed for trip ID = "
				+ crewDutyTripsDTO.getTripId() + " Payroll Number = " + crewDutyTripsDTO.getCrewID() + " \"}", 420);
		return response;
	}

}

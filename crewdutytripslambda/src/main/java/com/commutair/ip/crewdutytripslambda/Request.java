package com.commutair.ip.crewdutytripslambda;

import lombok.Data;

@Data
public class Request {
	private String payrollNumber;
	private String crewNumber;
	private String crewSsn;
	private String body;
}

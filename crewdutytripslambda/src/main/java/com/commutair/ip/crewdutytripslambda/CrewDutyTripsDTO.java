/**
 * 
 */
package com.commutair.ip.crewdutytripslambda;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

/**
 * @author Arun.Kumar
 *
 */
@Data
@DynamoDBTable(tableName = "DEV.CREW_DUTY_TRIPS")
public class CrewDutyTripsDTO {

	private String tripId;
	private String crewID;
	private String crewNumber;
	private String workCode;
	private String tripName;
	private String hotel;
	private String equipType;
	private String domicile;
	private String tripDate;
	private String changeType;
	private String tripOrigin;
	private String tripDestination;
	private String tripReportTime;
	private String tripReleaseTime;
	private String tripType;
	private String tripCarrierCode;
	private String tripSeatAssigned;
	private String tripFlightNumber;
	private String acNoseNumber;
	private String acTailNumber;
	private String tripDepartureCity;
	private String tripDepartureTime;
	private String actualDepartureTime;
	private String tripArrivalCity;
	private String tripArrivalTime;
	private String actualArrivalTime;
	private String reserveType;

	@DynamoDBHashKey
	private String businessKey;
	private String lastUpdatedDtTm;
	private String tripEnd;
	private String tripStart;
	private long tripTtl;

}

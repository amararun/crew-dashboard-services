package com.commutair.ip.crewdutytripslambda;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DutyTripsHandler implements RequestHandler<ApiGatewayProxyRequest, Response>, CommonConstants {
	private DynamoDBMapper dynamoDBMapper;
	private AmazonDynamoDB client;
	private LambdaLogger logger;
	// private Regions REGION = Regions.US_EAST_1;

	public Response handleRequest(ApiGatewayProxyRequest request, Context context) {
		Response response = null;
		// Initializing logger
		logger = context.getLogger();
		CrewDutyTripsDTO crewDutyTripsDTO = new CrewDutyTripsDTO();
		try {

			// Retrieving environment variables
			String dutyTripTableName = System.getenv("dutyTripTableName");
			String calendarTableName = System.getenv("calendarTableName");
			String ttlDays = System.getenv("ttlDays");
			String region = System.getenv("region");
			String endPoint = System.getenv("endPoint");

			// Initializing connection to DynamoDB
			this.initDynamoDbClient(region, endPoint);

			// Converting Json to object
			ObjectMapper mapper = new ObjectMapper();
			logger.log("message body - " + request.getBody());
			crewDutyTripsDTO = mapper.readValue(request.getBody(), CrewDutyTripsDTO.class);

			// Finding bid month to get crew calendar object
			String bidMonth = CommonUtil.findBidMonth(crewDutyTripsDTO.getTripDate());
			logger.log("Bid Month = " + bidMonth);

			// DELETE operation
			if ("D".equals(crewDutyTripsDTO.getChangeType())) {
				String businessKey = TRIP_KEY + DOUBLE_COLON + crewDutyTripsDTO.getCrewNumber() + DOUBLE_COLON
						+ crewDutyTripsDTO.getTripDate() + DOUBLE_COLON + crewDutyTripsDTO.getWorkCode() + DOUBLE_COLON
						+ crewDutyTripsDTO.getTripFlightNumber() + DOUBLE_COLON
						+ crewDutyTripsDTO.getTripDepartureCity() + DOUBLE_COLON
						+ crewDutyTripsDTO.getTripArrivalCity();
				crewDutyTripsDTO.setBusinessKey(businessKey);

				deleteData(crewDutyTripsDTO, dutyTripTableName);

				String calBusinessKey = CALENDAR_KEY + DOUBLE_COLON + crewDutyTripsDTO.getCrewNumber() + DOUBLE_COLON
						+ bidMonth;
				CrewCalendar crewCalendar = dynamoDBMapper.load(CrewCalendar.class, calBusinessKey);
				crewCalendar.getCrewActivities().remove(businessKey);

				persistData(crewCalendar, calendarTableName);

				response = CommonUtil.setResponse("{\"message\":\"Data deleted successfully\"}", 200);
			} else {
				// INSERT operation
				this.constructDutyTripsRelatedDetails(crewDutyTripsDTO, ttlDays);
				CrewCalendar crewCalendar = this.constructCalendarDetails(crewDutyTripsDTO, bidMonth, ttlDays);

				persistData(crewDutyTripsDTO, dutyTripTableName);
				persistData(crewCalendar, calendarTableName);

				response = CommonUtil.setResponse("{\"message\":\"Data saved successfully\"}", 200);
			}
			logger.log("Process Ends ");
		} catch (Exception e) {
			response = CommonUtil.handleException(e, crewDutyTripsDTO);
		}

		return response;

	}

	private void persistData(Object objectDTO, String tableName) throws ConditionalCheckFailedException {
		logger.log("Entering persistData function");
		dynamoDBMapper.save(objectDTO, new TableNameOverride(tableName).config());
		logger.log("Exit persistData function");
	}

	private void deleteData(Object objectDTO, String tableName) throws ConditionalCheckFailedException {
		logger.log("Entering deleteData function");
		dynamoDBMapper.delete(objectDTO, new TableNameOverride(tableName).config());
		logger.log("Exit persistData function");
	}

	private CrewCalendar constructCalendarDetails(CrewDutyTripsDTO crewDutyTripsDTO, String bidMonth, String ttlDays)
			throws NumberFormatException, ParseException {
		logger.log("Entering in to constructCalendarDetails method");
		String businessKey = CALENDAR_KEY + DOUBLE_COLON + crewDutyTripsDTO.getCrewNumber() + DOUBLE_COLON + bidMonth;
		List<String> crewActivities = null;
		CrewCalendar crewCalendar = dynamoDBMapper.load(CrewCalendar.class, businessKey);
		if (crewCalendar == null) {
			crewCalendar = new CrewCalendar();
			crewCalendar.setBusinessKey(businessKey);
			crewActivities = new ArrayList<String>();
			long ttl = CommonUtil.getTTLUnits(crewDutyTripsDTO.getTripDate(), ISO_DATE_FORMAT,
					Integer.valueOf(ttlDays));
			crewCalendar.setTtl(ttl);
		} else {
			crewActivities = crewCalendar.getCrewActivities();
		}
		if (!crewActivities.contains(crewDutyTripsDTO.getBusinessKey())) {
			crewActivities.add(crewDutyTripsDTO.getBusinessKey());
			crewCalendar.setCrewActivities(crewActivities);
		}

		logger.log("Exit constructCalendarDetails method");
		return crewCalendar;

	}

	private void constructDutyTripsRelatedDetails(CrewDutyTripsDTO crewDutyTripsDTO, String ttlDays)
			throws ParseException {
		logger.log("Entering in to constructDutyTripsRelatedDetails method");
		long tripsTTL = CommonUtil.getTTLUnits(crewDutyTripsDTO.getTripDate(), ISO_DATE_FORMAT,
				Integer.valueOf(ttlDays));
		String tripDate = crewDutyTripsDTO.getTripDate();
		String businessKey = TRIP_KEY + DOUBLE_COLON + crewDutyTripsDTO.getCrewNumber() + DOUBLE_COLON + tripDate
				+ DOUBLE_COLON + crewDutyTripsDTO.getWorkCode() + DOUBLE_COLON + crewDutyTripsDTO.getTripFlightNumber()
				+ DOUBLE_COLON + crewDutyTripsDTO.getTripDepartureCity() + DOUBLE_COLON
				+ crewDutyTripsDTO.getTripArrivalCity();
		String tripStartISO = EMPTY_STRING;
		String tripEndISO = EMPTY_STRING;
		if (crewDutyTripsDTO.getTripDepartureTime().length() > 0
				&& crewDutyTripsDTO.getTripArrivalTime().length() > 0) {
			String tripStartDateTimeStamp = tripDate + SPACE + crewDutyTripsDTO.getTripDepartureTime();
			String tripEndDateTimeStamp = tripDate + SPACE + crewDutyTripsDTO.getTripArrivalTime();
			tripStartISO = CommonUtil.getCustomizedDate(tripStartDateTimeStamp, DATE_TIME_FORMAT_1,
					ISO_DATE_TIME_FORMAT);
			tripEndISO = CommonUtil.getCustomizedDate(tripEndDateTimeStamp, DATE_TIME_FORMAT_1, ISO_DATE_TIME_FORMAT);
		} else {
			tripStartISO = CommonUtil.getCustomizedDate(tripDate, ISO_DATE_FORMAT, ISO_DATE_TIME_FORMAT);
			tripEndISO = CommonUtil.getCustomizedDate(tripDate, ISO_DATE_FORMAT, ISO_DATE_TIME_FORMAT);
			crewDutyTripsDTO.setTripDepartureTime(DEFAULT_START_TIME);
			crewDutyTripsDTO.setTripArrivalTime(DEFAULT_START_TIME);
		}

		crewDutyTripsDTO.setBusinessKey(businessKey);
		crewDutyTripsDTO.setTripDate(tripDate);
		crewDutyTripsDTO.setTripStart(tripStartISO);
		crewDutyTripsDTO.setTripEnd(tripEndISO);
		crewDutyTripsDTO.setLastUpdatedDtTm(CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
		crewDutyTripsDTO.setTripTtl(tripsTTL);
		logger.log("Exit constructDutyTripsRelatedDetails method");
	}

	private void initDynamoDbClient(String region, String endPoint) {

		if (this.client == null) {
			logger.log("Creating dynamodb client");
			client = AmazonDynamoDBClientBuilder.standard()
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, region)).build();
		}
		if (this.dynamoDBMapper == null) {
			logger.log("Creating dynamodb mapper");
			this.dynamoDBMapper = new DynamoDBMapper(client);
		}
	}

	/*
	 * public static void main(String[] args) { DutyTripsHandler
	 * dutyTripsHandler=new DutyTripsHandler(); String
	 * json="  {\"acNoseNumber\": \"9157\",   \"acTailNumber\": \"N12157\",\"workCode\": \"\",   \"crewId\": \"\",   \"crewNumber\": \"\",   \"domicile\": \"\",   \"equipType\": \"\",   \"hotel\": \"N\",   \"isPlanned\": \"Yes\",   \"changeType\": \"U\",   \"taskEvent\": \"I\",   \"tripArrivalCity\": \"IAD\",   \"tripArrivalTime\": \"0722\",   \"tripCarrierCode\": \"C5\",   \"tripDate\": \"01/31/2018\",   \"tripDepartureCity\": \"FAY\",   \"tripDepartureTime\": \"0600\",   \"tripDestination\": \"EWR\",\"tripFlightNumber\": \"4961\",   \"tripId\": \"04/05/2018YG\",   \"tripName\": \"YG3\",   \"tripOrigin\": \"FAY\",   \"tripReleaseTime\": \"1427\",   \"tripReportTime\": \"0515\",   \"tripSeatAssigned\": \"1\",  \"tripType\": \"S\"}"
	 * ; dutyTripsHandler.handleRequest(json);
	 * json="  {\"acNoseNumber\": \"9157\",   \"acTailNumber\": \"N12157\",\"workCode\": \"W\",   \"crewId\": \"102952\",   \"crewNumber\": \"5681\",   \"domicile\": \"EWR\",   \"equipType\": \"XMJ\",   \"hotel\": \"N\",   \"isPlanned\": \"Yes\",   \"changeType\": \"U\",   \"taskEvent\": \"I\",   \"tripArrivalCity\": \"IAD\",   \"tripArrivalTime\": \"0722\",   \"tripCarrierCode\": \"C5\",   \"tripDate\": \"03/01/2018\",   \"tripDepartureCity\": \"FAY\",   \"tripDepartureTime\": \"0600\",   \"tripDestination\": \"EWR\",\"tripFlightNumber\": \"4961\",   \"tripId\": \"04/05/2018YG\",   \"tripName\": \"YG3\",   \"tripOrigin\": \"FAY\",   \"tripReleaseTime\": \"1427\",   \"tripReportTime\": \"0515\",   \"tripSeatAssigned\": \"1\",  \"tripType\": \"S\"}"
	 * ; dutyTripsHandler.handleRequest(json);
	 * json="  {\"acNoseNumber\": \"9157\",   \"acTailNumber\": \"N12157\",\"workCode\": \"W\",   \"crewId\": \"102952\",   \"crewNumber\": \"5681\",   \"domicile\": \"EWR\",   \"equipType\": \"XMJ\",   \"hotel\": \"N\",   \"isPlanned\": \"Yes\",   \"changeType\": \"U\",   \"taskEvent\": \"I\",   \"tripArrivalCity\": \"IAD\",   \"tripArrivalTime\": \"0722\",   \"tripCarrierCode\": \"C5\",   \"tripDate\": \"03/30/2018\",   \"tripDepartureCity\": \"FAY\",   \"tripDepartureTime\": \"0600\",   \"tripDestination\": \"EWR\",\"tripFlightNumber\": \"4961\",   \"tripId\": \"04/05/2018YG\",   \"tripName\": \"YG3\",   \"tripOrigin\": \"FAY\",   \"tripReleaseTime\": \"1427\",   \"tripReportTime\": \"0515\",   \"tripSeatAssigned\": \"1\",  \"tripType\": \"S\"}"
	 * ; dutyTripsHandler.handleRequest(json); }
	 */

}

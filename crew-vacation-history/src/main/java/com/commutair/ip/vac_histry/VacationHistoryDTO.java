package com.commutair.ip.vac_histry;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@DynamoDBTable(tableName = "")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VacationHistoryDTO {
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "CrewNumber")
	private String crewNumber;
	@DynamoDBAttribute(attributeName = "CrewID")
	private String crewID;
	@DynamoDBAttribute(attributeName = "Category")
	private String category;
	@DynamoDBAttribute(attributeName = "VacationDate")
	private String vacationDate;
	@DynamoDBAttribute(attributeName = "ChangeType")
	private String changeType;
	@DynamoDBAttribute(attributeName = "FromDate")
	private String fromDate;
	@DynamoDBAttribute(attributeName = "LastUpdatedDtTm")
	private String lastUpdatedDtTm;
	@DynamoDBAttribute(attributeName = "VacHistryTtl")
	private long vacHistryTtl;
	@DynamoDBRangeKey
	@DynamoDBAttribute(attributeName = "CreatedDtTmHistry")
	private String createdDtTmHistry;
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "ID")
	private String id;
}

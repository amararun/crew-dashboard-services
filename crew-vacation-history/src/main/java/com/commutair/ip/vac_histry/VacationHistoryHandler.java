package com.commutair.ip.vac_histry;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VacationHistoryHandler implements RequestHandler<DynamodbEvent, Void>, CommonConstants {
	private DynamoDB dynamoDB;
	private LambdaLogger logger;
	private AmazonDynamoDB client;
	private DynamoDBMapper dynamoDBMapper;

	public Void handleRequest(DynamodbEvent dynamodbEvent, Context context) {
		String ttlDays = System.getenv("ttlDays");
		String historyTableName = System.getenv("historyTableName");
		String region = System.getenv("region");
		String endPoint = System.getenv("endPoint");

		logger = context.getLogger();
		logger.log("Inside handle request method");
		for (DynamodbStreamRecord record : dynamodbEvent.getRecords()) {
			if (null == record.getDynamodb().getOldImage()) {
				continue;
			} else {
				Map<String, String> map = new HashMap<String, String>();
				for (Entry<String, AttributeValue> entry : record.getDynamodb().getOldImage().entrySet()) {
					String k = entry.getKey();
					String v = entry.getValue().getS();
					map.put(k, v);
				}

				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
				VacationHistoryDTO vacHistryDTO = mapper.convertValue(map, VacationHistoryDTO.class);
				logger.log("Data mapping has been processed successfully");
				setVacHistryDTO(vacHistryDTO, ttlDays);

				initDynamoDbClient(region, endPoint);
				save(vacHistryDTO, historyTableName);
			}
		}
		logger.log("Exit handle request method");
		return null;
	}

	private void save(Object object, String historyTableName) {
		logger.log("Inside save method");
		dynamoDBMapper.save(object,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(historyTableName).config());
		logger.log("Exit save method");
	}

	private void setVacHistryDTO(VacationHistoryDTO vacHistryDTO, String ttlDays) {
		logger.log("Inside setVacHistryDTO method");
		try {
			vacHistryDTO.setId("VacationHistory" + DOUBLE_COLON + vacHistryDTO.getVacationDate());
			vacHistryDTO.setVacHistryTtl(
					ServiceUtil.getTTLUnits(vacHistryDTO.getVacationDate(), ISO_DATE_FORMAT, Integer.valueOf(ttlDays)));
			vacHistryDTO.setCreatedDtTmHistry(ServiceUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
		} catch (NumberFormatException e) {
			ServiceUtil.getExceptionStackTrace(e);
		} catch (ParseException e) {
			ServiceUtil.getExceptionStackTrace(e);
		}
		logger.log("Exit setVacHistryDTO method");
	}

	private void initDynamoDbClient(String region, String endPoint) {
		if (null == this.client) {
			logger.log("Creating client object");
			client = AmazonDynamoDBClientBuilder.standard()
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, region)).build();
		}
		if (null == this.dynamoDB) {
			logger.log("Creating dynamodb object");
			this.dynamoDB = new DynamoDB(client);
		}
		if (null == this.dynamoDBMapper) {
			logger.log("Creating dynamodb mapper");
			this.dynamoDBMapper = new DynamoDBMapper(client);
		}
	}
}

package com.commutair.ip.vac_histry;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";
	String ISO_DATE_FORMAT = "yyyy-MM-dd";
}

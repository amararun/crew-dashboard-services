package com.commutair.ip.crewdbservices.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.commutair.ip.crewdbservices.dao.AircraftViewDAO;
import com.commutair.ip.crewdbservices.dao.CrewViewDAO;
import com.commutair.ip.crewdbservices.dto.AircraftViewDTO;
import com.commutair.ip.crewdbservices.dto.AircraftViewVO;
import com.commutair.ip.crewdbservices.dto.ArcftRequest;
import com.commutair.ip.crewdbservices.util.ServiceConstants;
import com.commutair.ip.crewdbservices.util.ServiceUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class AircraftViewService implements ServiceConstants {
	@Autowired
	AircraftViewDAO aircraftViewDAO;

	@Autowired
	CrewViewDAO crewCalendarViewDAO;

	public String getAircraftViewDetails(ArcftRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		String aircraftViewJson = EMPTY_STRING;
		Map<String, AircraftViewDTO> aircraftViewMap = new ConcurrentHashMap<>();
		int maxListSize = 0;
		String sysDate = ServiceUtil.getCurrentESTDate(CUR_DATE_FORMAT);
		if (EMPTY_STRING != request.getFlightNumber() && EMPTY_STRING != request.getAircraft()) {
			List<AircraftViewVO> crewDutyTripDTOs = aircraftViewDAO.getAircraftViewDetails2(sysDate,
					request.getAircraft(), request.getFlightNumber());
			getArcftDetails(aircraftViewMap, crewDutyTripDTOs, request);
		} else if (EMPTY_STRING != request.getFlightNumber()) {
			List<AircraftViewVO> crewDutyTripDTOs = aircraftViewDAO.getAircraftViewDetails(sysDate,
					request.getFlightNumber(), "tripFlightNumber");
			getArcftDetails(aircraftViewMap, crewDutyTripDTOs, request);
		} else if (EMPTY_STRING != request.getAircraft()) {
			List<AircraftViewVO> crewDutyTripDTOs = aircraftViewDAO.getAircraftViewDetails(sysDate,
					request.getAircraft(), "acTailNumber");
			getArcftDetails(aircraftViewMap, crewDutyTripDTOs, request);
		} else {
			List<AircraftViewVO> crewDutyTripDTOs = aircraftViewDAO.getAircraftViewDetails(sysDate);
			getArcftDetails(aircraftViewMap, crewDutyTripDTOs, request);
		}
		List<AircraftViewDTO> aircraftViewDTOs = new ArrayList<>(aircraftViewMap.values());
		if (EMPTY_STRING != request.getCrewNumber()) {
			List<AircraftViewDTO> aircraftViewDTOs2 = new ArrayList<>();
			for (AircraftViewDTO aircraftViewDTO : aircraftViewDTOs) {
				List<AircraftViewVO> aircraftViewVOs = new ArrayList<>();
				for (int i = 0; i < aircraftViewDTO.getArcftSchedList().size(); i++) {
					AircraftViewVO aircraftViewVO2 = aircraftViewDTO.getArcftSchedList().get(i);
					if (YES.equals(aircraftViewVO2.getCrewFlag())) {
						aircraftViewVOs.add(aircraftViewVO2);
					}
					if ((i + 1) == aircraftViewDTO.getArcftSchedList().size()) {
						if (0 < aircraftViewVOs.size()) {
							AircraftViewDTO aircraftViewDTO2 = new AircraftViewDTO();
							aircraftViewDTO2.setArcftSchedList(aircraftViewVOs);
							aircraftViewDTO2.setArcftNumber(aircraftViewDTO.getArcftNumber());
							aircraftViewDTOs2.add(aircraftViewDTO2);
						}
					}
				}

			}
			aircraftViewDTOs = aircraftViewDTOs2;
		}
		for (AircraftViewDTO aircraftViewDTO : aircraftViewDTOs) {
			aircraftViewDTO.setArcftSchedList(aircraftViewDTO.getArcftSchedList().stream()
					.sorted(Comparator.comparing(AircraftViewVO::getTripStart)).collect(Collectors.toList()));
			if (maxListSize < aircraftViewDTO.getArcftSchedList().size()) {
				maxListSize = aircraftViewDTO.getArcftSchedList().size();
			}
		}
		aircraftViewJson = ServiceUtil.convertObjectToJson(aircraftViewDTOs, HEADER_ARCFT_VIEW, maxListSize);
		return aircraftViewJson;
	}

	private void getArcftDetails(Map<String, AircraftViewDTO> aircraftViewMap, List<AircraftViewVO> crewDutyTripDTOs,
			ArcftRequest request) throws JsonProcessingException {
		crewDutyTripDTOs.stream().filter(crewDutyTripDto -> !checkForNullProperties(crewDutyTripDto))
				.forEach(crewDutyTripDto -> {
					String acTailNumber = crewDutyTripDto.getAcTailNumber();
					if (aircraftViewMap.containsKey(acTailNumber)) {
						updateArcftViewTripMap(aircraftViewMap, crewDutyTripDto, TRIP_API, request);
					} else {
						addArcftViewTripMap(aircraftViewMap, crewDutyTripDto, TRIP_API, request);
					}
				});

	}

	private void addArcftViewTripMap(Map<String, AircraftViewDTO> arcftViewMap, AircraftViewVO crewDutyTripDTO,
			String crewWorkStatus, ArcftRequest request) {
		boolean flag = checkForNullProperties(crewDutyTripDTO);

		if (!flag) {
			AircraftViewDTO aircraftViewDTO = new AircraftViewDTO();
			setSeat(crewDutyTripDTO);
			if (crewDutyTripDTO.getCrewNumber().equals(request.getCrewNumber())) {
				setCrewFlag(crewDutyTripDTO);
			}
			List<AircraftViewVO> crewDutyTripDTOs = new ArrayList<>();
			crewDutyTripDTOs.add(crewDutyTripDTO);
			aircraftViewDTO.setArcftSchedList(crewDutyTripDTOs);
			aircraftViewDTO.setArcftNumber(crewDutyTripDTO.getAcTailNumber());
			arcftViewMap.put(crewDutyTripDTO.getAcTailNumber(), aircraftViewDTO);
		}

	}

	private void updateArcftViewTripMap(Map<String, AircraftViewDTO> arcftViewMap, AircraftViewVO crewDutyTripDTO,
			String crewWorkStatus, ArcftRequest request) {

		boolean flag = checkForNullProperties(crewDutyTripDTO);

		if (!flag) {
			AircraftViewVO crewDutyTripDTO3 = null;

			List<AircraftViewVO> crewDutyTripDTOs = arcftViewMap.get(crewDutyTripDTO.getAcTailNumber())
					.getArcftSchedList();
			for (AircraftViewVO crewDutyTripDTO2 : crewDutyTripDTOs) {
				if (crewDutyTripDTO2.getTripFlightNumber().equals(crewDutyTripDTO.getTripFlightNumber())
						&& crewDutyTripDTO2.getTripDepartureCity().equals(crewDutyTripDTO.getTripDepartureCity())) {
					if (crewDutyTripDTO.getCrewNumber().equals(request.getCrewNumber())) {
						setCrewFlag(crewDutyTripDTO2);
					}
					setSeat(crewDutyTripDTO2, crewDutyTripDTO);
					crewDutyTripDTO3 = crewDutyTripDTO2;
				}
			}
			if (null != crewDutyTripDTO3) {
				crewDutyTripDTO = crewDutyTripDTO3;
			} else {
				if (crewDutyTripDTO.getCrewNumber().equals(request.getCrewNumber())) {
					setCrewFlag(crewDutyTripDTO);
				}
				setSeat(crewDutyTripDTO);
				crewDutyTripDTOs.add(crewDutyTripDTO);
			}

		}
	}

	private void setSeat(AircraftViewVO crewDutyTripDto) {
		// CrewProfileDTO2 crewProfileDTO =
		// crewCalendarViewDAO.getCrewProfileDetails(crewNumber);
		// if (null != crewProfileDTO) {
		if (PIC.equals(crewDutyTripDto.getTripSeatAssigned())) {
			crewDutyTripDto.setPic(crewDutyTripDto.getCrewNumber());
		} else if (SIC.equals(crewDutyTripDto.getTripSeatAssigned())) {
			crewDutyTripDto.setSic(crewDutyTripDto.getCrewNumber());
		} else if (FA.equals(crewDutyTripDto.getTripSeatAssigned())) {
			crewDutyTripDto.setFa(crewDutyTripDto.getCrewNumber());
		}
		// }

	}

	private void setSeat(AircraftViewVO crewDutyTripDto1, AircraftViewVO crewDutyTripDto2) {
		// CrewProfileDTO2 crewProfileDTO =
		// crewCalendarViewDAO.getCrewProfileDetails(crewNumber);
		// if (null != crewProfileDTO) {
		if (PIC.equals(crewDutyTripDto2.getTripSeatAssigned())) {
			crewDutyTripDto1.setPic(crewDutyTripDto2.getCrewNumber());
		} else if (SIC.equals(crewDutyTripDto2.getTripSeatAssigned())) {
			crewDutyTripDto1.setSic(crewDutyTripDto2.getCrewNumber());
		} else if (FA.equals(crewDutyTripDto2.getTripSeatAssigned())) {
			crewDutyTripDto1.setFa(crewDutyTripDto2.getCrewNumber());
		}
		// }

	}

	private boolean checkForNullProperties(AircraftViewVO crewDutyTripDto) {
		return Stream.of(crewDutyTripDto.getTripFlightNumber(), crewDutyTripDto.getTripDate(),
				crewDutyTripDto.getTripDepartureTime(), crewDutyTripDto.getTripDepartureCity(),
				crewDutyTripDto.getAcTailNumber(), crewDutyTripDto.getTripStart()).anyMatch(Objects::isNull);
	}

	private void setCrewFlag(AircraftViewVO aircraftViewVO) {
		if (null != aircraftViewVO.getTripSeatAssigned()) {
			aircraftViewVO.setCrewFlag(YES);
		}
	}
}

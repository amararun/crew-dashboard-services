package com.commutair.ip.crewdbservices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.commutair.ip.crewdbservices.dao.CrewAlertsDAO;
import com.commutair.ip.crewdbservices.util.ServiceUtil;

@Service
public class CrewAlertsService {
	@Autowired
	CrewAlertsDAO crewAlertsDao;

	public String getCrewAlerts(String date) {
		ItemCollection<ScanOutcome> itemCollection = crewAlertsDao.getCrewAlerts(date);
		String crewAlertsJson = ServiceUtil.convertItemsToJsonArray(itemCollection, "CrewAlerts");
		return crewAlertsJson;
	}
}

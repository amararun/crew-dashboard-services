/**
 * 
 */
package com.commutair.ip.crewdbservices.dto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "Dev.OpenTrips")
public class CrewOpenTripsDTO {

	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "ArrivalCity")
	private String arrivalCity;
	@DynamoDBAttribute(attributeName = "ArrivalTime")
	private String arrivalTime;
	@DynamoDBAttribute(attributeName = "ChangeType")
	private String changeType;
	@DynamoDBAttribute(attributeName = "DepartureTime")
	private String departureTime;
	@DynamoDBAttribute(attributeName = "DeparturtureCity")
	private String departurtureCity;
	@DynamoDBAttribute(attributeName = "EquipmentType")
	private String equipmentType;
	@DynamoDBIndexHashKey(globalSecondaryIndexName = "FlightDate-index")
	@DynamoDBAttribute(attributeName = "FlightDate")
	private String flightDate;
	@DynamoDBAttribute(attributeName = "FlightNumber")
	private String flightNumber;
	@DynamoDBAttribute(attributeName = "FlightType")
	private String flightType;
	@DynamoDBAttribute(attributeName = "Seat")
	private String seat;
	@DynamoDBIgnore
	private String pic;
	@DynamoDBIgnore
	private String sic;
	@DynamoDBIgnore
	private String fa;
	@DynamoDBAttribute(attributeName = "TripId")
	private String tripId;
	@DynamoDBAttribute(attributeName = "TripName")
	private String tripName;
	@DynamoDBIgnore
	private String reserveType;

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof CrewOpenTripsDTO)) {
			return false;
		}
		CrewOpenTripsDTO openTripsDTO = (CrewOpenTripsDTO) obj;
		return (this.flightNumber.equals(openTripsDTO.flightNumber) && this.flightDate.equals(openTripsDTO.flightDate));
	}

	@Override
	public int hashCode() {
		return (this.flightNumber + this.flightDate).hashCode();
	}

}
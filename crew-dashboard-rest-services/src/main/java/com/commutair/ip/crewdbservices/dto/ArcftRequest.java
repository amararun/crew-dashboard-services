package com.commutair.ip.crewdbservices.dto;

import lombok.Data;

@Data
public class ArcftRequest {
	private String date;
	private String flightNumber;
	// private String origin;
	// private String destination;
	private String crewNumber;
	private String aircraft;
}

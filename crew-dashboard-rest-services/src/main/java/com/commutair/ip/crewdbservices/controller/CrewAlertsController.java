/**
 * 
 */
package com.commutair.ip.crewdbservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.ip.crewdbservices.dto.Request;
import com.commutair.ip.crewdbservices.service.CrewAlertsService;

/**
 * @author Sathish.Ganapathi
 *
 */
@RestController
@CrossOrigin(origins = "*")
public class CrewAlertsController {

	@Autowired
	CrewAlertsService crewAlertsService;

	@PostMapping(value = "/crewAlerts")
	public ResponseEntity<String> getCrewAlerts(@RequestBody Request request) {
		String crewAlertsJson = crewAlertsService.getCrewAlerts(request.getDate());
		if (crewAlertsJson.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<String>(crewAlertsJson, HttpStatus.OK);
	}

}

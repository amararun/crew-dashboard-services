package com.commutair.ip.crewdbservices.service;

import static com.commutair.ip.crewdbservices.util.ServiceUtil.convertObjectToJson;
import static com.commutair.ip.crewdbservices.util.ServiceUtil.convertStringToLocalDate;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.commutair.ip.crewdbservices.crewviewdto.CrewDutyTripDTO;
import com.commutair.ip.crewdbservices.dao.OpenTripsDAO;
import com.commutair.ip.crewdbservices.dto.CrewOpenTripsDTO;
import com.commutair.ip.crewdbservices.dto.CrewOpenTripsRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class OpenTripsService {
	@Autowired
	OpenTripsDAO openTripsDao;

	public String getOpenTrips(CrewOpenTripsRequest request) throws JsonProcessingException, ParseException {

		List<String> dates = getDateRange(request.getStartDate(), request.getEndDate());

		List<CrewOpenTripsDTO> itemCollectionOpenTrips = new ArrayList<CrewOpenTripsDTO>();

		dates.forEach((date) -> {
			itemCollectionOpenTrips.addAll(openTripsDao.getCrewOpenTrips(date));
		});
		Set<CrewOpenTripsDTO> openTripsDTOs = new HashSet<>(itemCollectionOpenTrips);
		Iterator<CrewOpenTripsDTO> iterator = openTripsDTOs.iterator();
		while (iterator.hasNext()) {
			CrewOpenTripsDTO openTripsDTO = iterator.next();
			String tripFlightNumber = openTripsDTO.getFlightNumber();
			String tripDate = openTripsDTO.getFlightDate();
			if (tripFlightNumber != null && tripDate != null) {
				List<CrewDutyTripDTO> itemCollectionDutyTrips = openTripsDao.getCrewDutyTrips(openTripsDTO);
				int count = 0;
				for (CrewDutyTripDTO dutyTripDTO : itemCollectionDutyTrips) {
					String crewNumber = dutyTripDTO.getCrewNumber();
					String dutyTripSeat = dutyTripDTO.getTripSeatAssigned();

					if (dutyTripSeat != null && dutyTripSeat.equalsIgnoreCase("C")) {
						openTripsDTO.setPic(crewNumber);
						count = count + 1;
					} else if (dutyTripSeat != null && dutyTripSeat.equalsIgnoreCase("F")) {
						openTripsDTO.setSic(crewNumber);
						count = count + 1;
					} else if (dutyTripSeat != null && dutyTripSeat.equalsIgnoreCase("1")) {
						openTripsDTO.setFa(crewNumber);
						count = count + 1;
					}
					// if (null == dutyTripSeat || dutyTripSeat.isEmpty())
					// C -> PIC , F -> SIC and 1->FA
				}
				if (count == 3) {
					iterator.remove();
				}

			}
		}

		List<CrewOpenTripsDTO> openTripsDTOs_1 = openTripsDTOs.stream().sorted(
				Comparator.comparing(CrewOpenTripsDTO::getFlightDate).thenComparing(CrewOpenTripsDTO::getDepartureTime))
				.collect(Collectors.toList());

		return convertObjectToJson(openTripsDTOs_1, "openTrip");
	}

	public static List<String> getDateRange(String fromDate, String toDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate startDate = convertStringToLocalDate(fromDate);
		LocalDate endDate = convertStringToLocalDate(toDate);
		long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
		List<LocalDate> localDates = IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween + 1)
				.mapToObj(i -> startDate.plusDays(i)).collect(Collectors.toList());
		return localDates.stream().map(date -> date.format(formatter)).collect(Collectors.toList());
	}

}

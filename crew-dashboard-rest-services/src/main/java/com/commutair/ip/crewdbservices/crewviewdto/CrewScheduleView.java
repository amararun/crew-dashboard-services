package com.commutair.ip.crewdbservices.crewviewdto;

public interface CrewScheduleView {
	String getTimeStamp();

	String getStartTime();

	String getCrewWorkStatus();

}

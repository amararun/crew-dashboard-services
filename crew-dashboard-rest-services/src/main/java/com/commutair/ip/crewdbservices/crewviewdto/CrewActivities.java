package com.commutair.ip.crewdbservices.crewviewdto;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

import lombok.Data;

@Data
@DynamoDBDocument
public class CrewActivities {
	@DynamoDBAttribute(attributeName = "CrewActivities")
	private List<String> crewActivities;
	private String date;
}

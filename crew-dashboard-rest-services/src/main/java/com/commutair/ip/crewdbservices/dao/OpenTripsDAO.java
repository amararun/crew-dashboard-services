package com.commutair.ip.crewdbservices.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.commutair.ip.crewdbservices.crewviewdto.CrewDutyTripDTO;
import com.commutair.ip.crewdbservices.dto.CrewOpenTripsDTO;

@Component
public class OpenTripsDAO {
	@Autowired
	DynamoDB dynamoDB;
	@Autowired
	DynamoDBMapper dynamoDBMapper;
	@Value("${openTripTableName}")
	String openTripTableName;
	@Value("${tripTableNm}")
	String tripTableNm;

	public List<CrewOpenTripsDTO> getCrewOpenTrips(String flightDate) {

		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(flightDate));
		eav.put(":val2", new AttributeValue().withS("D"));

		DynamoDBQueryExpression<CrewOpenTripsDTO> queryExpression = new DynamoDBQueryExpression<CrewOpenTripsDTO>()
				.withIndexName("FlightDate-index").withConsistentRead(false)
				.withKeyConditionExpression("FlightDate=:val1").withFilterExpression("FlightType<>:val2")
				.withExpressionAttributeValues(eav);
		List<CrewOpenTripsDTO> crewOpenTripsDTOs = dynamoDBMapper.query(CrewOpenTripsDTO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(openTripTableName).config());
		return crewOpenTripsDTOs;
	}

	public List<CrewDutyTripDTO> getCrewDutyTrips(CrewOpenTripsDTO openTripsDTO) {

		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(openTripsDTO.getFlightNumber()));
		eav.put(":val2", new AttributeValue().withS(openTripsDTO.getFlightDate()));
		eav.put(":val3", new AttributeValue().withS("D"));
		eav.put(":val4", new AttributeValue().withS(openTripsDTO.getDeparturtureCity()));
		eav.put(":val5", new AttributeValue().withS(openTripsDTO.getArrivalCity()));

		DynamoDBQueryExpression<CrewDutyTripDTO> queryExpression = new DynamoDBQueryExpression<CrewDutyTripDTO>()
				.withIndexName("fltNumber-tripStart-index").withConsistentRead(false)
				.withKeyConditionExpression("tripFlightNumber = :val1")
				.withFilterExpression(
						"tripDate = :val2 and tripType<>:val3 and tripDepartureCity=:val4 and tripArrivalCity=:val5")
				.withExpressionAttributeValues(eav);
		List<CrewDutyTripDTO> crewDutyTripDTOs = dynamoDBMapper.query(CrewDutyTripDTO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTOs;
	}

}

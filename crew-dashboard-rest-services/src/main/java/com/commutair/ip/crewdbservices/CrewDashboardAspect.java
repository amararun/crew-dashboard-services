package com.commutair.ip.crewdbservices;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CrewDashboardAspect {
	private static final Logger logger = LoggerFactory.getLogger(CrewDashboardAspect.class);

	@Before(value = "allMethods()")
	public void beforeAdvice(JoinPoint joinPoint) {
		logger.info("Before method " + joinPoint.toShortString());
	}

	@AfterReturning(value = "allMethods()")
	public void afterReturnAdvice(JoinPoint joinPoint) {
		logger.info("After method " + joinPoint.toShortString());
	}

	@AfterThrowing(pointcut = "within(com.commutair.ip.crewdbservices.controller.*)")
	public void afterThrownAdvice(JoinPoint joinPoint) {
		logger.info("An exception has been thrown after " + joinPoint.toShortString());
	}

	@Pointcut("execution(* com.commutair.ip.crewdbservices.controller.*.*(..))")
	public void allMethods() {
	}
}

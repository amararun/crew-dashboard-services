package com.commutair.ip.crewdbservices.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.commutair.ip.crewdbservices.crewviewdto.CrewCalendarDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewProfileDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewRequest;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewVO;
import com.commutair.ip.crewdbservices.dao.CrewViewDAO;
import com.commutair.ip.crewdbservices.model.CrewViewFlightModel;
import com.commutair.ip.crewdbservices.model.CrewViewModel;
import com.commutair.ip.crewdbservices.util.ServiceConstants;
import com.commutair.ip.crewdbservices.util.ServiceUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class CrewViewService implements ServiceConstants {
	@Autowired
	CrewViewDAO crewCalendarViewDAO;
	@Autowired
	CrewViewModel crewViewModel;
	@Autowired
	CrewViewFlightModel flightModel;

	public String getCrewViewDetails(CrewViewRequest request)
			throws JsonParseException, JsonMappingException, IOException, ParseException {
		String crewViewJson = EMPTY_STRING;
		if (null != request.getCrewNumber() && request.getCrewNumber().length() > 0) {
			crewViewJson = getCrewViewDetailsForCrew(request);
		} else if (null != request.getFlightNumber() && request.getFlightNumber().length() > 0) {
			crewViewJson = flightModel.getCrewViewDetailsForFlight(request);
		} else {
			crewViewJson = getCrewViewDetailsForDate(request);
		}
		return crewViewJson;
	}

	private String getCrewViewDetailsForCrew(CrewViewRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		// int maxListSize = 0;
		CrewViewVO crewViewVO = null;
		// Map<String, CrewViewVO2> crewViewMap = new ConcurrentHashMap<>();
		CrewProfileDTO crewProfileDTO = crewCalendarViewDAO.getCrewProfileDetails(request.getCrewNumber());
		String crewCalendarKey = CLNDR_KEY + DOUBLE_COLON + request.getCrewNumber() + DOUBLE_COLON
				+ request.getBidMonth();
		CrewCalendarDTO crewCalendarDTO = crewCalendarViewDAO.getSingleCrewCalendarDetails(crewCalendarKey);
		crewViewVO = crewViewModel.getCrewViewVO(crewCalendarDTO, crewProfileDTO, request);
		List<CrewViewVO> crewViewDTOs = new ArrayList<>();
		crewViewDTOs.add(crewViewVO);
		// maxListSize = findMaxListSize(crewViewDTOs, maxListSize);
		return ServiceUtil.convertObjectToJson(crewViewDTOs, HEADER_CREW_VIEW, crewViewVO.getCrewScheduleList().size());
	}

	private String getCrewViewDetailsForDate(CrewViewRequest request)
			throws JsonParseException, JsonMappingException, IOException, ParseException {
		// Map<String, CrewViewVO2> crewViewMap = new ConcurrentHashMap<>();
		List<CrewViewVO> crewViewDTOs = null;
		List<CrewProfileDTO> crewProfileDTOs = crewCalendarViewDAO.getCrewProfileDetails();
		// List<CrewCalendarDTO2> crewCalendarDTOs = bidMonthCalculation(request);
		List<CrewCalendarDTO> crewCalendarDTOs = crewCalendarViewDAO.getCrewCalendarDetails(request.getBidMonth());
		int maxListSize = 0;
		// for (CrewProfileDTO crewProfileDTO : crewCaptainProfileDTOs) {
		// maxListSize = getCrewViewDTOs(crewCalendarDTOs, crewProfileDTO, request,
		// crewViewMap, maxListSize);
		crewViewDTOs = crewProfileDTOs.parallelStream().map(crewProfileDTO -> {
			CrewViewVO crewViewVO = null;
			if ((request.getBase().equals(crewProfileDTO.getDomicile())
					&& request.getSeat().equals(crewProfileDTO.getSeat()))) {
				crewViewVO = iterateCalendarDTOs(crewCalendarDTOs, crewProfileDTO, request);
			} else if ((request.getBase().equals(ALL) && request.getSeat().equals(ALL))) {
				crewViewVO = iterateCalendarDTOs(crewCalendarDTOs, crewProfileDTO, request);
			} else if ((request.getBase().equals(crewProfileDTO.getDomicile()) && request.getSeat().equals(ALL))) {
				crewViewVO = iterateCalendarDTOs(crewCalendarDTOs, crewProfileDTO, request);
			} else if ((request.getBase().equals(ALL) && request.getSeat().equals(crewProfileDTO.getSeat()))) {
				crewViewVO = iterateCalendarDTOs(crewCalendarDTOs, crewProfileDTO, request);
			}
			return crewViewVO;
		}).filter(crewViewVO -> null != crewViewVO).collect(Collectors.toList());

		// List<CrewViewVO2> crewViewDTOs = new ArrayList<>(crewViewMap.values());
		maxListSize = findMaxListSize(crewViewDTOs, maxListSize);
		return ServiceUtil.convertObjectToJson(crewViewDTOs, HEADER_CREW_VIEW, maxListSize);

	}

	/*
	 * private List<CrewCalendarDTO> bidMonthCalculation(CrewViewRequest request)
	 * throws ParseException { List<CrewCalendarDTO> crewCalendarDTOs = new
	 * ArrayList<>();
	 * crewCalendarDTOs.addAll(crewCalendarViewDAO.getCrewCalendarDetails(request.
	 * getBidMonth())); if (0 < ServiceUtil.getMonthRange(request.getStartDate(),
	 * request.getEndDate())) { crewCalendarDTOs
	 * .addAll(crewCalendarViewDAO.getCrewCalendarDetails(ServiceUtil.findBidMonth(
	 * request.getEndDate()))); } return crewCalendarDTOs; }
	 */

	public CrewViewVO iterateCalendarDTOs(List<CrewCalendarDTO> crewCalendarDTOs, CrewProfileDTO crewProfileDTO,
			CrewViewRequest request) {
		CrewViewVO crewViewVO = null;
		CrewCalendarDTO calendarDTO = crewCalendarDTOs.stream().filter(calendarDTO2 -> {
			String[] businessKey = calendarDTO2.getBusinessKey().split(DOUBLE_COLON);
			return (new Integer(businessKey[1]).toString()).equals(crewProfileDTO.getCrewNumber());
		}).findFirst().orElse(null);
		if (null != calendarDTO) {
			crewViewVO = crewViewModel.getCrewViewVO(calendarDTO, crewProfileDTO, request);
		}
		return crewViewVO;
	}

	private int findMaxListSize(List<CrewViewVO> crewViewVOs, int maxListSize) {

		/*
		 * for (CrewViewVO2 crewViewVO : crewViewVOs) { if (maxListSize <
		 * crewViewVO.getCrewScheduleList().size()) { maxListSize =
		 * crewViewVO.getCrewScheduleList().size(); } }
		 */
		maxListSize = crewViewVOs.stream().mapToInt(crewViewVO -> crewViewVO.getCrewScheduleList().size()).max()
				.orElse(0);
		return maxListSize;
	}

}

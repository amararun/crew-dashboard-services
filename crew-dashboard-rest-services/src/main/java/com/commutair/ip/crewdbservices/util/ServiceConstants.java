package com.commutair.ip.crewdbservices.util;

public interface ServiceConstants {
	String YES = "Y";
	String NO = "N";
	String HH_MM = "HHmm";
	String TRIP_API = "Trip";
	String VAC_API = "Vacation";
	String TRAIN_API = "Training";
	String TRIP_KEY = "CrewDutyTrip";
	String VAC_KEY = "CrewVacation";
	String TRAIN_KEY = "CrewTraining";
	String TRIP_RSRV_KEY = "::R::";
	String PROFILE_KEY = "CrewProfile";
	String IOE_VAL = "IOE";
	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";
	String CLNDR_KEY = "CrewClndr";
	String HEADER_CREW_VIEW = "crewView";
	String HEADER_ARCFT_VIEW = "aircraftView";
	String ALL = "All";
	String TRIP_RSRV_API = "Reserve";
	String CUR_DATE_FORMAT = "yyyy-MM-dd";
	String SIC = "F";
	String PIC = "C";
	String FA = "1";
}

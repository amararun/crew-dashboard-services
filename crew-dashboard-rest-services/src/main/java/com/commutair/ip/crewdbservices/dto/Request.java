package com.commutair.ip.crewdbservices.dto;

public class Request {
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}

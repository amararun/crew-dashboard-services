package com.commutair.ip.crewdbservices.crewviewdto;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@DynamoDBTable(tableName = "")
@Data
public class CrewCalendarDTO {
	@DynamoDBHashKey
	private String businessKey;
	private List<String> crewActivities;
}

package com.commutair.ip.crewdbservices.dto;

import lombok.Data;

@Data
public class CrewOpenTripsRequest {
	private String startDate;
	private String endDate;
}

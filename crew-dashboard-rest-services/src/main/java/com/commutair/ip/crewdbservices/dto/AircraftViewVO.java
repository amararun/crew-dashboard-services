package com.commutair.ip.crewdbservices.dto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "")
public class AircraftViewVO {
	private String crewNumber;
	private String tripDate;
	@DynamoDBIndexHashKey(globalSecondaryIndexName = "tripDate-tripStart-index")
	private String tripFlightNumber;
	private String tripDepartureCity;
	private String tripArrivalCity;
	private String tripDepartureTime;
	@DynamoDBIndexRangeKey(globalSecondaryIndexName = "tripDate-tripStart-index")
	private String tripStart;
	@DynamoDBHashKey
	private String businessKey;
	private String acTailNumber;
	private String pic;
	private String sic;
	private String fa;
	private String tripArrivalTime;
	private String tripSeatAssigned;
	private String crewFlag;
}

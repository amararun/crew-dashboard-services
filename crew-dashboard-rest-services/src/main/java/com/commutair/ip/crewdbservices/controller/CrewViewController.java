package com.commutair.ip.crewdbservices.controller;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.ip.crewdbservices.crewviewdto.CrewViewRequest;
import com.commutair.ip.crewdbservices.service.CrewViewService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@CrossOrigin(origins = "*")
public class CrewViewController {
	@Autowired
	CrewViewService crewViewService;

	@PostMapping(value = "/crewCalendarViewTest")
	public ResponseEntity<String> getCrewView(@RequestBody CrewViewRequest request)
			throws JsonParseException, JsonMappingException, IOException, ParseException {
		String crewCalendarViewJson = crewViewService.getCrewViewDetails(request);
		if (crewCalendarViewJson.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<String>(crewCalendarViewJson, HttpStatus.OK);
	}
}

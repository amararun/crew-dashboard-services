package com.commutair.ip.crewdbservices.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.commutair.ip.crewdbservices.dto.AircraftViewVO;

@Component
public class AircraftViewDAO {
	@Autowired
	DynamoDB dynamoDB;
	@Autowired
	DynamoDBMapper dynamoDBMapper;
	@Value("${tripTableNm}")
	private String tripTableNm;

	public List<AircraftViewVO> getAircraftViewDetails(String date) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(date));
		eav.put(":val2", new AttributeValue().withS("D"));

		DynamoDBQueryExpression<AircraftViewVO> queryExpression = new DynamoDBQueryExpression<AircraftViewVO>()
				.withIndexName("tripDate-tripStart-index").withConsistentRead(false)
				.withKeyConditionExpression("tripDate=:val1").withFilterExpression("tripType<>:val2")
				.withExpressionAttributeValues(eav);
		List<AircraftViewVO> crewDutyTripDTOs = dynamoDBMapper.query(AircraftViewVO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTOs;
	}

	public List<AircraftViewVO> getAircraftViewDetails(String date, String colValue, String colName) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(date));
		eav.put(":val2", new AttributeValue().withS(colValue));
		eav.put(":val3", new AttributeValue().withS("D"));
		DynamoDBQueryExpression<AircraftViewVO> queryExpression = new DynamoDBQueryExpression<AircraftViewVO>()
				.withIndexName("tripDate-tripStart-index").withConsistentRead(false)
				.withKeyConditionExpression("tripDate=:val1")
				.withFilterExpression(colName + "=:val2 and tripType<>:val3").withExpressionAttributeValues(eav);
		List<AircraftViewVO> crewDutyTripDTOs = dynamoDBMapper.query(AircraftViewVO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTOs;
	}

	public List<AircraftViewVO> getAircraftViewDetails2(String date, String aircraft, String fltNbr) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(date));
		eav.put(":val2", new AttributeValue().withS(aircraft));
		eav.put(":val3", new AttributeValue().withS(fltNbr));
		eav.put(":val4", new AttributeValue().withS("D"));
		DynamoDBQueryExpression<AircraftViewVO> queryExpression = new DynamoDBQueryExpression<AircraftViewVO>()
				.withIndexName("tripDate-tripStart-index").withConsistentRead(false)
				.withKeyConditionExpression("tripDate=:val1")
				.withFilterExpression("acTailNumber=:val2 and tripFlightNumber=:val3 and tripType<>:val4")
				.withExpressionAttributeValues(eav);
		List<AircraftViewVO> crewDutyTripDTOs = dynamoDBMapper.query(AircraftViewVO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTOs;
	}

}

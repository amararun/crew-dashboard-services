/**
 * 
 */
package com.commutair.ip.crewdbservices.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONArray;
import org.json.JSONObject;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.commutair.ip.crewdbservices.dto.CrewOpenTripsDTO;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Sathish.Ganapathi
 *
 */
public class ServiceUtil {

	public static String convertItemsToJsonArray(ItemCollection<ScanOutcome> itemCollection, String objectType) {
		String jsonString = "";
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		for (Item item : itemCollection) {
			jsonArray.put(new JSONObject(item.toJSON()));
		}
		jsonObject.put(objectType, jsonArray);
		jsonString = jsonObject.toString();
		return jsonString;
	}

	public static String convertItemsToJsonArray(ItemCollection<QueryOutcome> itemCollection) {
		String jsonString = "";
		if (itemCollection.iterator().hasNext()) {
			JSONArray jsonArray = new JSONArray();
			for (Item item : itemCollection) {
				jsonArray.put(new JSONObject(item.toJSON()));
			}
			jsonString = jsonArray.toString();
		}
		return jsonString;
	}

	public static List<String> getDateRange(String fromDate, String toDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate startDate = convertStringToLocalDate(fromDate);
		LocalDate endDate = convertStringToLocalDate(toDate);
		long numOfDaysBetween = (ChronoUnit.DAYS.between(startDate, endDate)) + 1;
		List<LocalDate> localDates = IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween)
				.mapToObj(i -> startDate.plusDays(i)).collect(Collectors.toList());
		return localDates.stream().map(date -> date.format(formatter)).collect(Collectors.toList());

	}

	public static long getMonthRange(String fromDate, String toDate) {
		LocalDate startDate = convertStringToLocalDate(fromDate).withDayOfMonth(1);
		LocalDate endDate = convertStringToLocalDate(toDate).withDayOfMonth(1);
		return ChronoUnit.MONTHS.between(startDate, endDate);
	}

	public static LocalDate convertStringToLocalDate(String date) {
		// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		// convert String to LocalDate
		return LocalDate.parse(date);
	}

	public static String convertObjectToJson(Object object, String objectType, int maxListSize)
			throws JsonProcessingException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(objectType, object);
		jsonObject.accumulate("maxListSize", maxListSize);
		return jsonObject.toString();
	}

	public static String getCurrentESTDate(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZoneId newYokZoneId = ZoneId.of("America/New_York");
		ZonedDateTime zonedDateTime = dateTime.atZone(newYokZoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(zonedDateTime);
	}

	public static String findBidMonth(String date) throws ParseException {
		// String tripDate=getCustomizedDate(tripDate, "MM/dd/yyyy",
		// "yyyy-MM-dd");
		Date tripDt = getDate(date, "yyyy-MM-dd");
		String year = getCustomizedDate(date, "yyyy-MM-dd", "yyyy");
		if (tripDt.compareTo(getDate("01/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("01/30/" + year, "MM/dd/yyyy")) <= 0)
			return "01/" + year;
		if (tripDt.compareTo(getDate("01/31/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("03/01/" + year, "MM/dd/yyyy")) <= 0)
			return "02/" + year;
		if (tripDt.compareTo(getDate("03/02/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("03/31/" + year, "MM/dd/yyyy")) <= 0)
			return "03/" + year;
		if (tripDt.compareTo(getDate("04/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("04/30/" + year, "MM/dd/yyyy")) <= 0)
			return "04/" + year;
		if (tripDt.compareTo(getDate("05/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("05/31/" + year, "MM/dd/yyyy")) <= 0)
			return "05/" + year;
		if (tripDt.compareTo(getDate("06/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("06/30/" + year, "MM/dd/yyyy")) <= 0)
			return "06/" + year;
		if (tripDt.compareTo(getDate("07/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("07/31/" + year, "MM/dd/yyyy")) <= 0)
			return "07/" + year;
		if (tripDt.compareTo(getDate("08/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("08/31/" + year, "MM/dd/yyyy")) <= 0)
			return "08/" + year;
		if (tripDt.compareTo(getDate("09/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("09/30/" + year, "MM/dd/yyyy")) <= 0)
			return "09/" + year;
		if (tripDt.compareTo(getDate("10/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("10/31/" + year, "MM/dd/yyyy")) <= 0)
			return "10/" + year;
		if (tripDt.compareTo(getDate("11/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("11/30/" + year, "MM/dd/yyyy")) <= 0)
			return "11/" + year;
		if (tripDt.compareTo(getDate("12/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(getDate("12/31/" + year, "MM/dd/yyyy")) <= 0)
			return "12/" + year;
		return null;
	}

	public static Date getDate(String date, String format) throws ParseException {
		SimpleDateFormat dataPointFormat = new SimpleDateFormat(format);
		Date dpDate = dataPointFormat.parse(date);
		return dpDate;
	}

	public static String getCustomizedDate(String date, String fromFormat, String toFormat) throws ParseException {
		SimpleDateFormat dataPointFormat = new SimpleDateFormat(fromFormat);
		Date dpDate = dataPointFormat.parse(date);
		SimpleDateFormat mintFormat = new SimpleDateFormat(toFormat);
		String mintDate = mintFormat.format(dpDate);
		return mintDate;
	}

	public static String convertObjectToJson(List<CrewOpenTripsDTO> openTripsDto, String objectType)
			throws JsonProcessingException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(objectType, openTripsDto);

		return jsonObject.toString();
	}
}

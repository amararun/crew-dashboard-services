package com.commutair.ip.crewdbservices.crewviewdto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "CrewTraining")
public class CrewTrainingDTO implements CrewScheduleView {

	private String crewNumber;
	@DynamoDBAttribute(attributeName = "fromDate")
	private String timeStamp;
	private String releaseTime;
	@DynamoDBAttribute(attributeName = "reportTime")
	private String startTime;
	private String scheduledDate;
	@DynamoDBIgnore
	private String crewWorkStatus;
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "businessKey")
	private String businessKey;
	private String workCode;

	public String getCrewNumber() {
		return crewNumber;
	}

	public void setCrewNumber(String crewNumber) {
		this.crewNumber = crewNumber;
	}

	@Override
	@DynamoDBAttribute(attributeName = "fromDate")
	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}

	@Override
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	@Override
	public String getCrewWorkStatus() {
		return crewWorkStatus;
	}

	public void setCrewWorkStatus(String crewWorkStatus) {
		this.crewWorkStatus = crewWorkStatus;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getWorkCode() {
		return workCode;
	}

	public void setWorkCode(String workCode) {
		this.workCode = workCode;
	}

}

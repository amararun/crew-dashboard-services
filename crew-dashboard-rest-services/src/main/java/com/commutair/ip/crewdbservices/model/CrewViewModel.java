package com.commutair.ip.crewdbservices.model;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.commutair.ip.crewdbservices.crewviewdto.CrewCalendarDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewDutyTripDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewProfileDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewScheduleView;
import com.commutair.ip.crewdbservices.crewviewdto.CrewTrainingDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewVacationDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewRequest;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewVO;
import com.commutair.ip.crewdbservices.dao.CrewViewDAO;
import com.commutair.ip.crewdbservices.util.ServiceConstants;
import com.commutair.ip.crewdbservices.util.ServiceUtil;

@Component
public class CrewViewModel implements ServiceConstants {
	@Autowired
	CrewViewDAO crewCalendarViewDAO;

	public CrewViewVO getCrewViewVO(CrewCalendarDTO crewCalendarDTO, CrewProfileDTO crewProfileDTO,
			CrewViewRequest request) {

		List<String> crewDutyTripKeys = new ArrayList<>();
		List<String> crewTrainingKeys = new ArrayList<>();
		List<String> crewVacationKeys = new ArrayList<>();
		List<String> crewReserveKeys = new ArrayList<>();

		getCrewCalendarKeys(request, crewCalendarDTO, crewDutyTripKeys, crewTrainingKeys, crewVacationKeys,
				crewReserveKeys);
		List<CrewScheduleView> crewScheduleViews = new ArrayList<>();
		if (request.getWorkStatus().equals(ALL)) {
			getCrewScheduleList(crewDutyTripKeys, crewTrainingKeys, crewVacationKeys, crewScheduleViews);
		} else {
			getCrewScheduleList(crewDutyTripKeys, crewTrainingKeys, crewVacationKeys, crewReserveKeys,
					crewScheduleViews, request.getWorkStatus());
		}
		CrewViewVO crewViewDTO = null;

		if (null != request.getStartTime() && request.getStartTime().length() > 0) {
			crewScheduleViews = filterCrewScheduleByTime(request, crewScheduleViews);
		}
		if (crewScheduleViews.size() > 0) {
			crewViewDTO = new CrewViewVO();
			crewViewDTO.setCrewProfileDTO(crewProfileDTO);
			crewViewDTO.setCrewScheduleList(crewScheduleViews.stream()
					.sorted(Comparator.comparing(CrewScheduleView::getTimeStamp)).collect(Collectors.toList()));
			// crewViewMap.put(crewProfileDTO.getCrewNumber(), crewViewDTO);
		}

		return crewViewDTO;
	}

	public List<CrewScheduleView> filterCrewScheduleByTime(CrewViewRequest request,
			List<CrewScheduleView> crewScheduleList) {
		List<CrewScheduleView> crewViewVOs = crewScheduleList.stream().map(crewViewVO -> {
			LocalTime reqStartTime = LocalTime.parse(request.getStartTime(), DateTimeFormatter.ofPattern(HH_MM));
			LocalTime reqEndTime = LocalTime.parse(request.getEndTime(), DateTimeFormatter.ofPattern(HH_MM));
			switch (crewViewVO.getCrewWorkStatus()) {
			case TRIP_API: {
				LocalTime startTime = LocalTime.parse(crewViewVO.getStartTime(), DateTimeFormatter.ofPattern(HH_MM));
				if (startTime.equals(reqStartTime) || startTime.equals(reqEndTime)
						|| (startTime.isAfter(reqStartTime) && startTime.isBefore(reqEndTime))) {
					return crewViewVO;
				}
				break;
			}
			case TRAIN_API: {
				LocalTime startTime = LocalTime.parse(crewViewVO.getStartTime(), DateTimeFormatter.ofPattern(HH_MM));
				if (startTime.equals(reqStartTime) || startTime.equals(reqEndTime)
						|| (startTime.isAfter(reqStartTime) && startTime.isBefore(reqEndTime))) {
					return crewViewVO;
				}
				break;
			}
			case VAC_API: {
				return crewViewVO;
			}
			}
			return null;
		}).filter(object -> null != object).collect(Collectors.toList());
		return crewViewVOs;
	}

	public void getCrewScheduleList(List<String> crewDutyTripKeys, List<String> crewTrainingKeys,
			List<String> crewVacationKeys, List<CrewScheduleView> crewScheduleList) {
		getTripReservDetails(crewDutyTripKeys, crewScheduleList);
		getTrainingDetails(crewTrainingKeys, crewScheduleList);
		getVacationDetails(crewVacationKeys, crewScheduleList);
	}

	public void getCrewScheduleList(List<String> crewDutyTripKeys, List<String> crewTrainingKeys,
			List<String> crewVacationKeys, List<String> crewReserveKeys, List<CrewScheduleView> crewScheduleList,
			String workStatus) {
		switch (workStatus) {
		case TRIP_API:
			getTripReservDetails(crewDutyTripKeys, crewScheduleList);
			break;
		case TRAIN_API:
			getTrainingDetails(crewTrainingKeys, crewScheduleList);
			break;
		case VAC_API:
			getVacationDetails(crewVacationKeys, crewScheduleList);
			break;
		case TRIP_RSRV_API:
			getTripReservDetails(crewReserveKeys, crewScheduleList);
			break;
		}

	}

	private void getVacationDetails(List<String> keys, List<CrewScheduleView> crewScheduleList) {
		if (keys.size() > 0) {
			crewScheduleList.addAll(keys.parallelStream().map(key -> {
				CrewVacationDTO crewVacationDTO = crewCalendarViewDAO.getCrewVacation(key);
				crewVacationDTO.setCrewWorkStatus(VAC_API);
				return crewVacationDTO;
			}).filter(object -> null != object.getTimeStamp()).collect(Collectors.toList()));
		}
	}

	private void getTrainingDetails(List<String> keys, List<CrewScheduleView> crewScheduleList) {
		if (keys.size() > 0) {
			crewScheduleList.addAll(keys.parallelStream().map(key -> {
				CrewTrainingDTO crewTrainingDTO = crewCalendarViewDAO.getCrewTraining(key);
				crewTrainingDTO.setCrewWorkStatus(TRAIN_API);
				return crewTrainingDTO;
			}).filter(object -> null != object.getTimeStamp()).collect(Collectors.toList()));
		}
	}

	private void getTripReservDetails(List<String> keys, List<CrewScheduleView> crewScheduleList) {
		if (keys.size() > 0) {
			crewScheduleList.addAll(keys.parallelStream().map(key -> {
				CrewDutyTripDTO crewDutyTripDTO = crewCalendarViewDAO.getCrewDutyTrips(key);
				crewDutyTripDTO.setCrewWorkStatus(TRIP_API);
				crewDutyTripDTO.setIoeInd((IOE_VAL.equalsIgnoreCase(crewDutyTripDTO.getIoeInd())) ? YES : NO);
				return crewDutyTripDTO;
			}).filter(object -> null != object.getTimeStamp()).collect(Collectors.toList()));
		}
	}

	public void getCrewCalendarKeys(CrewViewRequest request, CrewCalendarDTO crewCalendarDTO,
			List<String> crewDutyTripKeys, List<String> crewTrainingKeys, List<String> crewVacationKeys,
			List<String> crewReserveKeys) {
		List<String> dates = ServiceUtil.getDateRange(request.getStartDate(), request.getEndDate());
		if (request.getWorkStatus().equals(ALL)) {
			crewCalendarDTO.getCrewActivities().forEach((key) -> {
				if (dates.stream().anyMatch(date -> key.contains(date))) {
					if (key.contains(TRIP_KEY)) {
						crewDutyTripKeys.add(key);
					} else if (key.contains(TRAIN_KEY)) {
						crewTrainingKeys.add(key);
					} else if (key.contains(VAC_KEY)) {
						crewVacationKeys.add(key);
					}
				}
			});
		} else {
			crewCalendarDTO.getCrewActivities().forEach((key) -> {
				if (dates.stream().anyMatch(date -> key.contains(date))) {
					if (key.contains(TRAIN_KEY)) {
						crewTrainingKeys.add(key);
					} else if (key.contains(VAC_KEY)) {
						crewVacationKeys.add(key);
					} else if (key.contains(TRIP_KEY) && key.contains(TRIP_RSRV_KEY)) {
						crewReserveKeys.add(key);
					} else if (key.contains(TRIP_KEY)) {
						crewDutyTripKeys.add(key);
					}
				}
			});
		}
	}
}

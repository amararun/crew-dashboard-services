package com.commutair.ip.crewdbservices.crewviewdto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;
@Data
@DynamoDBTable(tableName="")
public class CrewProfileDTO {
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "CqfInd")
	private String cqfInd;
	@DynamoDBAttribute(attributeName = "CrewNumber")
	@DynamoDBIndexHashKey(globalSecondaryIndexName = "crewNumber-index")
	private String crewNumber;
	@DynamoDBAttribute(attributeName = "Domicile")
	private String domicile;
	@DynamoDBAttribute(attributeName = "Seat")
	private String seat;
	@DynamoDBAttribute(attributeName = "LineChkPilotInd")
	private String lineChkPilotInd;
	@DynamoDBAttribute(attributeName = "SeniorityNumber")
	private String seniorityNumber;
}


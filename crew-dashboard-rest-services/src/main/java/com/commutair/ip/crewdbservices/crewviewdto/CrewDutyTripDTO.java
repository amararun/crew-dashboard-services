package com.commutair.ip.crewdbservices.crewviewdto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@DynamoDBTable(tableName = "CrewDutyTrips")
@Data
public class CrewDutyTripDTO implements CrewScheduleView {

	private String crewNumber;
	private String tripDate;
	private String tripFlightNumber;
	private String tripDepartureCity;
	private String tripArrivalCity;
	@DynamoDBAttribute(attributeName = "tripDepartureTime")
	private String startTime;
	@DynamoDBAttribute(attributeName = "tripStart")
	private String timeStamp;
	@DynamoDBIgnore
	private String crewWorkStatus;
	@DynamoDBHashKey
	private String businessKey;
	@DynamoDBAttribute(attributeName = "tripName")
	private String ioeInd;
	private String reserveType;
	private String workCode;
	private String tripSeatAssigned;
	private String tripReportTime;
	private String tripReleaseTime;
}

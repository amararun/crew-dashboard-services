package com.commutair.ip.crewdbservices.dto;

import java.util.List;

import lombok.Data;

@Data
public class AircraftViewDTO {
	private String arcftNumber;
	private List<AircraftViewVO> arcftSchedList;
}

package com.commutair.ip.crewdbservices.crewviewdto;

import lombok.Data;

@Data
public class CrewViewRequest {
	private String startDate;
	private String endDate;
	private String searchType;
	private String bidMonth;
	private String seat;
	private String base;
	private String startTime;
	private String endTime;
	private String crewNumber;
	private String flightNumber;
	private String workStatus;
}

package com.commutair.ip.crewdbservices.crewviewdto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "CrewVacation")
public class CrewVacationDTO implements CrewScheduleView {
	@DynamoDBAttribute(attributeName = "FromDate")
	private String timeStamp;
	@DynamoDBAttribute(attributeName = "VacationDate")
	private String vacDate;
	@DynamoDBAttribute(attributeName = "CrewNumber")
	private String crewNumber;
	@DynamoDBIgnore
	private String crewWorkStatus;
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "Category")
	private String category;

	@Override
	public String getStartTime() {
		// TODO Auto-generated method stub
		return null;
	}
}

package com.commutair.ip.crewdbservices.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.ip.crewdbservices.dto.CrewOpenTripsRequest;
import com.commutair.ip.crewdbservices.service.OpenTripsService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@CrossOrigin(origins = "*")
public class OpenTripsController {

	@Autowired
	OpenTripsService openTripsService;

	@PostMapping(value = "/crewOpenTrips")
	public ResponseEntity<String> getOpenTrips(@RequestBody CrewOpenTripsRequest request)
			throws JsonProcessingException, ParseException {
		String crewOpenTripsJsonFinal = openTripsService.getOpenTrips(request);
		if (crewOpenTripsJsonFinal.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<String>(crewOpenTripsJsonFinal, HttpStatus.OK);
	}
}

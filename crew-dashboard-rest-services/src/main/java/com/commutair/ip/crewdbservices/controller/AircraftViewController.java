package com.commutair.ip.crewdbservices.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.ip.crewdbservices.dto.ArcftRequest;
import com.commutair.ip.crewdbservices.service.AircraftViewService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@CrossOrigin(origins = "*")
public class AircraftViewController {
	@Autowired
	AircraftViewService aircraftViewService;

	@PostMapping(value = "/aircraftView")
	public ResponseEntity<String> getAircraftView(@RequestBody ArcftRequest arcftRequest)
			throws JsonParseException, JsonMappingException, IOException {
		String aircraftViewJson = aircraftViewService.getAircraftViewDetails(arcftRequest);

		if (aircraftViewJson.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<String>(aircraftViewJson, HttpStatus.OK);
	}
}

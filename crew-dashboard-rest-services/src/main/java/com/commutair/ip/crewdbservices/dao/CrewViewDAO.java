package com.commutair.ip.crewdbservices.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.commutair.ip.crewdbservices.crewviewdto.CrewCalendarDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewDutyTripDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewProfileDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewTrainingDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewVacationDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewRequest;
import com.commutair.ip.crewdbservices.util.ServiceConstants;

@Component
public class CrewViewDAO implements ServiceConstants {
	@Autowired
	DynamoDB dynamoDB;
	@Autowired
	DynamoDBMapper dynamoDBMapper;
	@Value("${vacTableNm}")
	private String vacTableNm;
	@Value("${tripTableNm}")
	private String tripTableNm;
	@Value("${calTableNm}")
	private String calTableNm;
	@Value("${profileTableNm}")
	private String profileTableNm;
	@Value("${trainTableNm}")
	private String trainTableNm;

	public List<CrewProfileDTO> getCrewProfileDetails() {

		List<CrewProfileDTO> crewProfileDTOs = dynamoDBMapper.parallelScan(CrewProfileDTO.class,
				new DynamoDBScanExpression(), 50,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(profileTableNm).config());
		return crewProfileDTOs;
	}

	public CrewProfileDTO getCrewProfileDetails(String crewNumber) {
		String hashKey = PROFILE_KEY + DOUBLE_COLON + crewNumber;
		CrewProfileDTO crewProfileDTO = dynamoDBMapper.load(CrewProfileDTO.class, hashKey,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(profileTableNm).config());
		return crewProfileDTO;
	}

	public List<CrewCalendarDTO> getCrewCalendarDetails(String bidMonth) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(bidMonth));
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
				.withFilterExpression("contains(businessKey,:val1)").withExpressionAttributeValues(eav);
		List<CrewCalendarDTO> crewCalendarDTOs = dynamoDBMapper.parallelScan(CrewCalendarDTO.class, scanExpression, 50,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(calTableNm).config());
		return crewCalendarDTOs;
	}

	public CrewCalendarDTO getSingleCrewCalendarDetails(String businessKey) {
		CrewCalendarDTO crewCalendarDTO = dynamoDBMapper.load(CrewCalendarDTO.class, businessKey,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(calTableNm).config());
		return crewCalendarDTO;
	}

	public CrewDutyTripDTO getCrewDutyTrips(String hashKey) {
		CrewDutyTripDTO crewDutyTripDTO = dynamoDBMapper.load(CrewDutyTripDTO.class, hashKey,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTO;
		/*
		 * CrewDutyTripDTO partitionKey = new CrewDutyTripDTO();
		 * partitionKey.setBusinessKey(hashKey);
		 * DynamoDBQueryExpression<CrewDutyTripDTO> queryExpression = new
		 * DynamoDBQueryExpression<CrewDutyTripDTO>() .withHashKeyValues(partitionKey);
		 * List<CrewDutyTripDTO> crewDutyTripDTOs =
		 * dynamoDBMapper.query(CrewDutyTripDTO.class, queryExpression,
		 * DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).
		 * config()); return (crewDutyTripDTOs.size() > 0) ? crewDutyTripDTOs.get(0) :
		 * new CrewDutyTripDTO();
		 */
	}

	public CrewTrainingDTO getCrewTraining(String hashKey) {
		CrewTrainingDTO crewTrainingDTO = dynamoDBMapper.load(CrewTrainingDTO.class, hashKey,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(trainTableNm).config());
		return crewTrainingDTO;
	}

	public CrewVacationDTO getCrewVacation(String hashKey) {
		CrewVacationDTO crewVacationDTO = dynamoDBMapper.load(CrewVacationDTO.class, hashKey,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(vacTableNm).config());
		return crewVacationDTO;
	}

	public void getCrewVacation(List<String> crewVacationKeys) {
		Map<String, List<Object>> crewVacationDTO2s = dynamoDBMapper.batchLoad(crewVacationKeys,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(vacTableNm).config());
		for (Entry<String, List<Object>> entry : crewVacationDTO2s.entrySet()) {
			String k = entry.getKey();
			List<Object> v = entry.getValue();
			System.out.println(k + "-" + v.size());
		}
	}

	public List<CrewDutyTripDTO> getCrewFlightDetails(CrewViewRequest request) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(request.getFlightNumber()));
		if (null != request.getStartTime() && request.getStartTime().length() > 0) {

			StringBuilder startTime = new StringBuilder(request.getStartTime()).insert(0, 'T').insert(3, ':');
			StringBuilder endTime = new StringBuilder(request.getEndTime()).insert(0, 'T').insert(3, ':');
			eav.put(":val2", new AttributeValue().withS(request.getStartDate() + startTime));
			eav.put(":val3", new AttributeValue().withS(request.getEndDate() + endTime));
		} else {
			eav.put(":val2", new AttributeValue().withS(request.getStartDate() + "T00:00"));
			eav.put(":val3", new AttributeValue().withS(request.getEndDate() + "T23:59"));
		}
		DynamoDBQueryExpression<CrewDutyTripDTO> queryExpression = new DynamoDBQueryExpression<CrewDutyTripDTO>()
				.withIndexName("fltNumber-tripStart-index").withConsistentRead(false)
				.withKeyConditionExpression("tripFlightNumber=:val1 and tripStart between :val2 and :val3")
				.withExpressionAttributeValues(eav);
		List<CrewDutyTripDTO> crewDutyTripDTOs = dynamoDBMapper.query(CrewDutyTripDTO.class, queryExpression,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tripTableNm).config());
		return crewDutyTripDTOs;
	}

}

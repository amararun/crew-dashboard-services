package com.commutair.ip.crewdbservices.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;

@Component
public class CrewAlertsDAO {
	@Autowired
	DynamoDB dynamoDB;

	public ItemCollection<ScanOutcome> getCrewAlerts(String date) {
		Table table = dynamoDB.getTable("CrewAlerts");

		ItemCollection<ScanOutcome> items = table.scan();

		return items;
	}
}

package com.commutair.ip.crewdbservices.crewviewdto;

import java.util.List;

import lombok.Data;

@Data
public class CrewViewVO {
	private CrewProfileDTO crewProfileDTO;
	private List<CrewScheduleView> crewScheduleList;

}

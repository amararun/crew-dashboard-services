package com.commutair.ip.crewdbservices.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.commutair.ip.crewdbservices.crewviewdto.CrewDutyTripDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewProfileDTO;
import com.commutair.ip.crewdbservices.crewviewdto.CrewScheduleView;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewRequest;
import com.commutair.ip.crewdbservices.crewviewdto.CrewViewVO;
import com.commutair.ip.crewdbservices.dao.CrewViewDAO;
import com.commutair.ip.crewdbservices.util.ServiceConstants;
import com.commutair.ip.crewdbservices.util.ServiceUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Component
public class CrewViewFlightModel implements ServiceConstants {
	@Autowired
	CrewViewDAO crewCalendarViewDAO;

	public String getCrewViewDetailsForFlight(CrewViewRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		Map<String, CrewViewVO> crewViewMap = new ConcurrentHashMap<>();
		int maxListSize = 0;
		List<CrewDutyTripDTO> crewDutyTripDTOs = crewCalendarViewDAO.getCrewFlightDetails(request);
		crewDutyTripDTOs.stream().forEach(crewDutyTripDto -> {
			String crewNumber = crewDutyTripDto.getCrewNumber();
			if (crewViewMap.containsKey(crewNumber)) {
				updateCrewViewTripMap(crewViewMap, crewDutyTripDto);
			} else {
				addCrewViewTripMap(crewViewMap, crewDutyTripDto);
			}
		});
		List<CrewViewVO> crewViewDTOs = new ArrayList<>(crewViewMap.values());
		for (CrewViewVO crewViewVO : crewViewDTOs) {
			if (maxListSize < crewViewVO.getCrewScheduleList().size()) {
				maxListSize = crewViewVO.getCrewScheduleList().size();
				crewViewVO.setCrewScheduleList(crewViewVO.getCrewScheduleList().stream()
						.sorted(Comparator.comparing(CrewScheduleView::getTimeStamp)).collect(Collectors.toList()));

			}
		}
		return ServiceUtil.convertObjectToJson(crewViewDTOs, HEADER_CREW_VIEW, maxListSize);
	}

	private Map<String, CrewViewVO> addCrewViewTripMap(Map<String, CrewViewVO> crewViewMap,
			CrewDutyTripDTO crewDutyTripDTO) {

		List<CrewDutyTripDTO> crewDutyTripDTOs = new ArrayList<>();
		boolean flag = checkForNullProperties(crewDutyTripDTO);

		if (!flag) {
			crewDutyTripDTO.setCrewWorkStatus(TRIP_API);
			crewDutyTripDTO.setIoeInd((IOE_VAL.equalsIgnoreCase(crewDutyTripDTO.getIoeInd())) ? YES : NO);
			crewDutyTripDTOs.add(crewDutyTripDTO);
			crewViewMap.put(crewDutyTripDTO.getCrewNumber(), setCrewViewDto(crewDutyTripDTOs));
		}

		return crewViewMap;
	}

	private void updateCrewViewTripMap(Map<String, CrewViewVO> crewViewMap, CrewDutyTripDTO crewDutyTripDto) {

		boolean flag = checkForNullProperties(crewDutyTripDto);

		if (!flag) {
			crewDutyTripDto.setCrewWorkStatus(TRIP_API);
			List<CrewScheduleView> crewViews = crewViewMap.get(crewDutyTripDto.getCrewNumber()).getCrewScheduleList();
			crewViews.add(crewDutyTripDto);
		}

	}

	private boolean checkForNullProperties(CrewDutyTripDTO crewDutyTripDto) {
		return Stream
				.of(crewDutyTripDto.getTripFlightNumber(), crewDutyTripDto.getTripDate(),
						crewDutyTripDto.getStartTime(), crewDutyTripDto.getTripDepartureCity())
				.anyMatch(Objects::isNull);
	}

	private CrewViewVO setCrewViewDto(List<CrewDutyTripDTO> crewDutyTripDTOs) {
		CrewViewVO crewViewVO = new CrewViewVO();
		List<CrewScheduleView> crewViews = new ArrayList<>();
		crewViews.addAll(crewDutyTripDTOs);
		crewViewVO.setCrewScheduleList(crewViews);
		CrewProfileDTO crewProfileDTO = crewCalendarViewDAO
				.getCrewProfileDetails(crewDutyTripDTOs.get(0).getCrewNumber());
		crewViewVO.setCrewProfileDTO(crewProfileDTO);
		return crewViewVO;
	}
}

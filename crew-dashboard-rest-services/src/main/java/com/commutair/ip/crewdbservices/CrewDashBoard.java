/**
 * 
 */
package com.commutair.ip.crewdbservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Sathish.Ganapathi
 *
 */
@SpringBootApplication
@ImportResource("classpath:application-context.xml")
@ComponentScan(basePackages = "com.commutair.ip.crewdbservices")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class CrewDashBoard extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CrewDashBoard.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(CrewDashBoard.class, args);
	}

}

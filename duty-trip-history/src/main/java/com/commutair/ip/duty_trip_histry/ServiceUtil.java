package com.commutair.ip.duty_trip_histry;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ServiceUtil {
	public static long getTTLUnits(String date, String format, int numberOfDays) throws ParseException {
		LocalDateTime dateTime = LocalDateTime.now();
		dateTime.plusDays(numberOfDays);
		return dateTime.atZone(ZoneId.of("America/New_York")).toEpochSecond();
	}

	public static String getCurrentESTTime(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZonedDateTime utcTime = dateTime.atZone(ZoneOffset.UTC);
		ZonedDateTime estTime = utcTime.withZoneSameInstant(ZoneId.of("America/New_York"));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(estTime);
	}

	public static String getExceptionStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}
}

/**
 * 
 */
package com.commutair.ip.duty_trip_histry;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author Arun.Kumar
 *
 */
@Data
@DynamoDBTable(tableName = "DEV.CREW_DUTY_TRIPS_HISTORY")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrewDutyTripHistoryDTO {

	private String acNoseNumber;
	private String acTailNumber;
	@DynamoDBHashKey
	private String businessKey;
	private String category;
	@DynamoDBRangeKey
	private String createdDate;
	private String crewId;
	private String crewNumber;
	private String domicile;
	private String equipType;
	private String hotel;
	private String isPlanned;
	private String changeType;
	private String tripArrivalCity;
	private String tripArrivalTime;
	private String tripCarrierCode;
	private String tripDate;
	private String tripDepartureCity;
	private String tripDepartureTime;
	private String tripDestination;
	private String tripEnd;
	private String tripFlightNumber;
	private String tripId;
	private String tripName;
	private String tripOrigin;
	private String tripReleaseTime;
	private String tripReportTime;
	private String tripSeatAssigned;
	private String tripStart;
	private String tripType;
	private long tripTtl;
	private String flightKey;

}

package com.commutair.ip.duty_trip_histry;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";
	String TRIP_HISTORY_KEY = "CrewDutyTripHistory";
	String ISO_DATE_FORMAT = "yyyy-MM-dd";
}

package com.commutair.ip.crewopentripsapi;

import static com.commutair.ip.crewopentripsapi.CommonUtil.handleException;
import static com.commutair.ip.crewopentripsapi.CommonUtil.setResponse;

import java.io.IOException;
import java.text.ParseException;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CrewOpenTripsHandler implements RequestHandler<Request, Response>, CommonConstants {
	private DynamoDB dynamoDB;
	private LambdaLogger logger;
	private AmazonDynamoDB client;
	private DynamoDBMapper dynamoDBMapper;

	public Response handleRequest(Request request, Context context) {
		Response response = null;
		// Initializing logger
		logger = context.getLogger();
		try {
			// Retrieving environment variables
			String tableName = System.getenv("tableName");
			String ttlDays = System.getenv("ttlDays");
			String region = System.getenv("region");
			String endPoint = System.getenv("endPoint");

			// Initializing connection to DynamoDB
			this.initDynamoDbClient(region, endPoint);

			// Converting Json to object
			ObjectMapper mapper = new ObjectMapper();
			CrewOpenTripsDTO crewOpenTripsDTO;
			crewOpenTripsDTO = mapper.readValue(request.getBody(), CrewOpenTripsDTO.class);

			// Setting time to live and business key to insert/update records
			String businessKey = OPEN_TRIP_KEY + DOUBLE_COLON + crewOpenTripsDTO.getFlightDate() + DOUBLE_COLON
					+ crewOpenTripsDTO.getFlightNumber() + DOUBLE_COLON + crewOpenTripsDTO.getSeat();
			long ttl = CommonUtil.getTTLUnits(crewOpenTripsDTO.getFlightDate(), ISO_DATE_FORMAT,
					Integer.valueOf(ttlDays));

			crewOpenTripsDTO.setTtl(ttl);
			crewOpenTripsDTO.setLastModifiedDateTime(CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
			crewOpenTripsDTO.setBusinessKey(businessKey);

			// DELETE Operation
			if (crewOpenTripsDTO.getChangeType().equalsIgnoreCase(DELETE)) {
				try {
					deleteData(businessKey, tableName);
					response = setResponse("{\"message\":\"Data deleted successfully\"}", 200);
				} catch (Exception e) {
					logger.log(CommonUtil.getExceptionStackTrace(e));
					response = handleException(e, "{\"message\":\"Data failed during delete operation\"}");
				}
			} // INSERT Operation
			else {
				try {
					persistData(crewOpenTripsDTO, tableName);
					response = setResponse("{\"message\":\"Data saved successfully\"}", 200);
				} catch (Exception e) {
					logger.log(CommonUtil.getExceptionStackTrace(e));
					response = handleException(e, "{\"message\":\"Data failed during insert operation\"}");
				}
			}

		} catch (JsonMappingException e) {
			response = handleException(e, "{\"message\":\"JsonMappingException-Invalid Json\"}");
		} catch (IOException e) {
			response = handleException(e, "{\"message\":\"IOException-Error occured while reading Json\"}");
		} catch (NumberFormatException e) {
			response = handleException(e, "{\"message\":\"NumberFormatException-Error occured while reading Json\"}");
		} catch (ParseException e) {
			response = handleException(e, "{\"message\":\"ParseException-Error occured while reading Json\"}");
		}
		return response;

	}

	private void initDynamoDbClient(String region, String endPoint) {
		if (this.client == null) {
			logger.log("Creating client object");
			client = AmazonDynamoDBClientBuilder.standard()
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, region)).build();
		}
		if (this.dynamoDB == null) {
			logger.log("Creating dynamodb object");
			this.dynamoDB = new DynamoDB(client);
		}
		if (this.dynamoDBMapper == null) {
			logger.log("Entering in to null dynamo db mapper");
			this.dynamoDBMapper = new DynamoDBMapper(client);
		}
	}

	private void persistData(Object objectDTO, String tableName) {
		logger.log("Entering persistData function");
		dynamoDBMapper.save(objectDTO, new TableNameOverride(tableName).config());
	}

	private DeleteItemOutcome deleteData(String businessKey, String tableName) throws ConditionalCheckFailedException {
		logger.log("Entering deleteData function " + businessKey);
		return this.dynamoDB.getTable(tableName)
				.deleteItem(new DeleteItemSpec().withPrimaryKey(new PrimaryKey("BusinessKey", businessKey)));

	}

}

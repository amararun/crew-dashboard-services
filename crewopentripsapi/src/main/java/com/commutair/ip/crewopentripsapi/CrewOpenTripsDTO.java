/**
 * 
 */
package com.commutair.ip.crewopentripsapi;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "Dev.OpenTrips")
public class CrewOpenTripsDTO {

	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "ArrivalCity")
	private String arrivalCity;
	@DynamoDBAttribute(attributeName = "ArrivalTime")
	private String arrivalTime;
	@DynamoDBAttribute(attributeName = "ChangeType")
	private String changeType;
	@DynamoDBAttribute(attributeName = "DepartureTime")
	private String departureTime;
	@DynamoDBAttribute(attributeName = "DeparturtureCity")
	private String departurtureCity;
	@DynamoDBAttribute(attributeName = "EquipmentType")
	private String equipmentType;
	@DynamoDBAttribute(attributeName = "FlightDate")
	private String flightDate;
	@DynamoDBAttribute(attributeName = "FlightNumber")
	private String flightNumber;
	@DynamoDBAttribute(attributeName = "FlightType")
	private String flightType;
	@DynamoDBAttribute(attributeName = "Seat")
	private String seat;
	@DynamoDBAttribute(attributeName = "TripId")
	private String tripId;
	@DynamoDBAttribute(attributeName = "TripName")
	private String tripName;
	@DynamoDBAttribute(attributeName = "TTL")
	private long ttl;
	@DynamoDBAttribute(attributeName = "LastModifiedDateTime")
	private String lastModifiedDateTime;

}
package com.commutair.ip.crewopentripsapi;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class CommonUtil {
	/**
	 * This method converts current date in required format
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentESTTime(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZoneId newYokZoneId = ZoneId.of("America/New_York");
		ZonedDateTime zonedDateTime = dateTime.atZone(newYokZoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(zonedDateTime);
	}

	/**
	 * This method stack trace of given exception
	 * 
	 * @param e
	 * @return
	 */
	public static String getExceptionStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	public static Response handleException(Exception e, String jsonMessage) {
		Response response = null;
		response = setResponse(jsonMessage, 420);
		return response;
	}

	public static Response setResponse(String responseBody, int statusCode) {
		Response response = new Response();
		response.setBase64Encoded(false);
		response.setStatusCode(statusCode);
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("service-name", "crewvacation");
		response.setHeaders(headerMap);
		response.setBody(responseBody);
		return response;
	}

	public static long getTTLUnits(String date, String format, int numberOfDays) throws ParseException {
		LocalDateTime dateTime = LocalDateTime.now();
		dateTime.plusDays(numberOfDays);
		return dateTime.atZone(ZoneId.of("America/New_York")).toEpochSecond();
	}

}

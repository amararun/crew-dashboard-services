package com.commutair.ip.crewopentripsapi;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String ISO_DATE_FORMAT = "yyyy-MM-dd";

	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";

	String OPEN_TRIP_KEY = "OpenTrip";

	String DELETE = "D";

}

package com.ca.ip.alerthistory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;
import lombok.NoArgsConstructor;

@DynamoDBTable(tableName = "")
@Data
@NoArgsConstructor
public class AlertLastModDtTm {
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "LastModDtTm")
	private String lastModDtTm;
}

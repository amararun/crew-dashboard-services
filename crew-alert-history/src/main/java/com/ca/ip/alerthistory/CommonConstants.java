package com.ca.ip.alerthistory;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String EMPTY_STRING = "";
}

package com.ca.ip.alerthistory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@DynamoDBTable(tableName = "")
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrewAlertHistoryDTO implements CommonConstants {
	@DynamoDBAttribute(attributeName = "PayrollNumber")
	private String payrollNumber;
	@DynamoDBAttribute(attributeName = "TripDate")
	private String tripDate;
	@DynamoDBAttribute(attributeName = "WorkCode")
	private String workCode;
	@DynamoDBAttribute(attributeName = "FlightNum")
	private String flightNum;
	@DynamoDBAttribute(attributeName = "DepCity")
	private String depCity;
	@DynamoDBAttribute(attributeName = "ArrCity")
	private String arrCity;
	@DynamoDBAttribute(attributeName = "CrewNumber")
	private String crewNumber;
	@DynamoDBAttribute(attributeName = "Seat")
	private String seat;
	@DynamoDBAttribute(attributeName = "Base")
	private String base;
	@DynamoDBAttribute(attributeName = "TripCode")
	private String tripCode;
	@DynamoDBAttribute(attributeName = "TripUniKey")
	private String tripUniKey;
	@DynamoDBAttribute(attributeName = "Segment")
	private String segment;
	@DynamoDBAttribute(attributeName = "TotalFltsAffected")
	private String totalFltsAffected;
	@DynamoDBAttribute(attributeName = "ReportTime")
	private String reportTime;
	@DynamoDBAttribute(attributeName = "AlertCode")
	private String alertCode;
	@DynamoDBAttribute(attributeName = "AlertName")
	private String alertName;
	@DynamoDBAttribute(attributeName = "Reason")
	private String reason;
	@DynamoDBAttribute(attributeName = "AlertType")
	private String alertType;
	@DynamoDBAttribute(attributeName = "DateOfVioOrWarn")
	private String dateOfVioOrWarn;
	@DynamoDBAttribute(attributeName = "StartDateTimeOfVio")
	private String startDateTimeOfVio;
	@DynamoDBAttribute(attributeName = "MaxFdp")
	private String maxFdp;
	@DynamoDBAttribute(attributeName = "SchedFdp")
	private String schedFdp;
	@DynamoDBAttribute(attributeName = "CompletedFDP")
	private String completedFDP;
	@DynamoDBAttribute(attributeName = "ProjCompletedFdpForCrewInAir")
	private String projCompletedFdpForCrewInAir;
	@DynamoDBAttribute(attributeName = "RemFdp")
	private String remFdp;
	@DynamoDBAttribute(attributeName = "CumFdpIn168")
	private String cumFdpIn168;
	@DynamoDBAttribute(attributeName = "RemCumFdpIn168")
	private String remCumFdpIn168;
	@DynamoDBAttribute(attributeName = "CumFdpIn672")
	private String cumFdpIn672;
	@DynamoDBAttribute(attributeName = "RemCumFdpIn672")
	private String remCumFdpIn672;
	@DynamoDBAttribute(attributeName = "MaxFltTime")
	private String maxFltTime;
	@DynamoDBAttribute(attributeName = "ShedFltTime")
	private String shedFltTime;
	@DynamoDBAttribute(attributeName = "RemFltTime")
	private String remFltTime;
	@DynamoDBAttribute(attributeName = "MaxFltTimeIn672")
	private String maxFltTimeIn672;
	@DynamoDBAttribute(attributeName = "CumFltTimeIn672")
	private String cumFltTimeIn672;
	@DynamoDBAttribute(attributeName = "RemFltTimeIn672")
	private String remFltTimeIn672;
	@DynamoDBAttribute(attributeName = "MaxFltTimeInOneYr")
	private String maxFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "CumFltTimeInOneYr")
	private String cumFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "RemFltTimeInOneYr")
	private String remFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "ReqRest")
	private String reqRest;
	@DynamoDBAttribute(attributeName = "TotalRest")
	private String totalRest;
	@DynamoDBAttribute(attributeName = "_30HourRestIn168StartDtm")
	private String _30HourRestIn168StartDtm;
	@DynamoDBAttribute(attributeName = "Extension")
	private String extension;
	@DynamoDBAttribute(attributeName = "ExtnInMins")
	private String extnInMins;
	@DynamoDBAttribute(attributeName = "RestrictionCode")
	private String restrictionCode;
	@DynamoDBAttribute(attributeName = "ExpiringQual")
	private String expiringQual;
	@DynamoDBAttribute(attributeName = "CumStartDtm")
	private String cumStartDtm;
	@DynamoDBAttribute(attributeName = "CumEndDtm")
	private String cumEndDtm;
	@DynamoDBAttribute(attributeName = "CumStartFltNum")
	private String cumStartFltNum;
	@DynamoDBAttribute(attributeName = "CumStartFltDate")
	private String cumStartFltDate;
	@DynamoDBAttribute(attributeName = "CumStartFltDepStn")
	private String cumStartFltDepStn;
	@DynamoDBAttribute(attributeName = "CumStartFltDep")
	private String cumStartFltDep;
	@DynamoDBAttribute(attributeName = "CumStartFltArrStn")
	private String cumStartFltArrStn;
	@DynamoDBAttribute(attributeName = "CumStartFltArrTime")
	private String cumStartFltArrTime;
	@DynamoDBAttribute(attributeName = "CumEndFltNum")
	private String cumEndFltNum;
	@DynamoDBAttribute(attributeName = "CumEndFltDate")
	private String cumEndFltDate;
	@DynamoDBAttribute(attributeName = "CumEndFltDepStn")
	private String cumEndFltDepStn;
	@DynamoDBAttribute(attributeName = "CumEndFltDep")
	private String cumEndFltDep;
	@DynamoDBAttribute(attributeName = "CumEndFltArrStn")
	private String cumEndFltArrStn;
	@DynamoDBAttribute(attributeName = "CumEndFltArrTime")
	private String cumEndFltArrTime;
	@DynamoDBAttribute(attributeName = "CcoDtm")
	private String ccoDtm;
	@DynamoDBAttribute(attributeName = "CcoTmLeft")
	private String ccoTmLeft;
	@DynamoDBAttribute(attributeName = "LastFltNum")
	private String lastFltNum;
	@DynamoDBAttribute(attributeName = "LastFltDate")
	private String lastFltDate;
	@DynamoDBAttribute(attributeName = "LastFltDepStn")
	private String lastFltDepStn;
	@DynamoDBAttribute(attributeName = "LastFltDep")
	private String lastFltDep;
	@DynamoDBAttribute(attributeName = "LastFltArrStn")
	private String lastFltArrStn;
	@DynamoDBAttribute(attributeName = "LastFltArr")
	private String lastFltArr;
	@DynamoDBAttribute(attributeName = "LfMaxDepTime")
	private String lfMaxDepTime;
	@DynamoDBAttribute(attributeName = "LfMaxArrTime")
	private String lfMaxArrTime;
	@DynamoDBAttribute(attributeName = "LfFdp")
	private String lfFdp;
	@DynamoDBAttribute(attributeName = "ReqRestLf")
	private String reqRestLf;
	@DynamoDBAttribute(attributeName = "SchdRestLf")
	private String schdRestLf;
	@DynamoDBAttribute(attributeName = "OutButNotOff")
	private String outButNotOff;
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "FileName")
	private String fileName;
	@DynamoDBAttribute(attributeName = "CreatedDateTime")
	private String createdDateTime;
	@DynamoDBAttribute(attributeName = "ID")
	@DynamoDBHashKey
	private String id;
	@DynamoDBAttribute(attributeName = "TTL")
	private long ttl;
	@DynamoDBRangeKey
	@DynamoDBAttribute(attributeName = "CreatedDtTmHistry")
	private String createdDtTmHistry;
}

package com.ca.ip.alerthistory;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AlertHistoryHandler implements RequestHandler<DynamodbEvent, Void>, CommonConstants {
	private DynamoDB dynamoDB;
	private LambdaLogger logger;
	private AmazonDynamoDB client;
	private DynamoDBMapper dynamoDBMapper;

	@Override
	public Void handleRequest(DynamodbEvent dynamodbEvent, Context context) {
		try {
			boolean setModDtTmFlag = false;
			logger = context.getLogger();
			logger.log("Process Starts");
			initDynamoDbClient("us-east-1", "dynamodb.us-east-1.amazonaws.com");
			String ttlDays = System.getenv("ttlDays");
			String historyTableName = System.getenv("historyTableName");
			String alertLastModTableName = System.getenv("alertLastModTableName");
			for (DynamodbStreamRecord record : dynamodbEvent.getRecords()) {
				if (null == record.getDynamodb().getNewImage()) {
					continue;
				} else {
					logger.log(record.getDynamodb().getNewImage().toString());
					Map<String, String> map = new HashMap<String, String>();
					for (Entry<String, AttributeValue> entry : record.getDynamodb().getNewImage().entrySet()) {
						String k = entry.getKey();
						String v = entry.getValue().getS();
						map.put(k, v);
					}
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

					CrewAlertHistoryDTO alertHistoryDTO = mapper.convertValue(map, CrewAlertHistoryDTO.class);
					logger.log("Data mapping has been processed successfully");
					alertHistoryDTO.setId("AlertHistory::" + alertHistoryDTO.getCrewNumber() + "::"
							+ alertHistoryDTO.getDateOfVioOrWarn());
					alertHistoryDTO.setTtl(CommonUtil.getTTLUnits(alertHistoryDTO.getDateOfVioOrWarn(), "yyyy-MM-dd",
							Integer.valueOf(ttlDays)));
					alertHistoryDTO.setCreatedDtTmHistry(CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
					dynamoDBMapper.save(alertHistoryDTO,
							DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(historyTableName).config());
					setModDtTmFlag = true;
				}
			}
			if (setModDtTmFlag) {
				emptyAlertLastModTable(alertLastModTableName);
				AlertLastModDtTm lastModDtTm = new AlertLastModDtTm();
				lastModDtTm.setLastModDtTm(CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
				dynamoDBMapper.save(lastModDtTm, DynamoDBMapperConfig.TableNameOverride
						.withTableNameReplacement(alertLastModTableName).config());
			}
			logger.log("Process Ends");
		} catch (NumberFormatException e) {
			CommonUtil.getExceptionStackTrace(e);
		} catch (ParseException e) {
			CommonUtil.getExceptionStackTrace(e);
		}
		return null;
	}

	private void emptyAlertLastModTable(String tableName) {
		logger.log("Inside emptyCrewAlertsTable method in class CrewAlertsDAO");
		List<AlertLastModDtTm> alertLastModDtTms = dynamoDBMapper.scan(AlertLastModDtTm.class,
				new DynamoDBScanExpression(),
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName).config());
		logger.log("Total CrewAlerts records to be deleted - " + alertLastModDtTms.size());
		for (AlertLastModDtTm alertLastModDtTm : alertLastModDtTms) {
			dynamoDBMapper.delete(alertLastModDtTm,
					DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName).config());
		}
		logger.log("CrewAlerts records deleted successfully");
	}

	private void initDynamoDbClient(String region, String endPoint) {
		if (this.client == null) {
			logger.log("Creating client object");
			client = AmazonDynamoDBClientBuilder.standard()
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, region)).build();
		}
		if (this.dynamoDB == null) {
			logger.log("Creating dynamodb object");
			this.dynamoDB = new DynamoDB(client);
		}
		if (this.dynamoDBMapper == null) {
			logger.log("Creating dynamodb mapper");
			this.dynamoDBMapper = new DynamoDBMapper(client);
		}
	}
}

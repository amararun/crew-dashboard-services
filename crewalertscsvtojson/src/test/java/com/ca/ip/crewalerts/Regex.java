/**
 * 
 */
package com.ca.ip.crewalerts;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Sathish.Ganapathi
 *
 */
public class Regex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();

		names.add("10/30/2018");
		names.add("2018-10-30");
		names.add("10/30/2018T23:59"); // Incorrect

		String regex = "^[A-Z]{1}$";

		Pattern pattern = Pattern.compile(regex);

		for (String name : names) {
			try {
				// Matcher matcher = pattern.matcher(name);
				// System.out.println(matcher.matches());
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy'T'HH:mm");
				LocalDateTime.parse(name, formatter);
				System.out.println("valid date");
			} catch (DateTimeParseException e) {
				System.out.println("Invalid date");
			}
		}

		// String startDateTimeOfVio = "10/30/2018";
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("MM/dd/yyyy'T'HH:mm");
		// LocalDateTime.parse(startDateTimeOfVio, formatter);

	}

}

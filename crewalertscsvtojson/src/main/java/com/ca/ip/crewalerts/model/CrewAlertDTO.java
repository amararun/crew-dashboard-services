package com.ca.ip.crewalerts.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.ca.ip.crewalerts.util.CommonConstants;
import com.ca.ip.crewalerts.util.CommonUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBTable(tableName = "")
@JsonPropertyOrder({ "payrollNumber", "tripDate", "workCode", "flightNum", "depCity", "arrCity", "crewNumber", "seat",
		"base", "tripCode", "tripUniKey", "segment", "totalFltsAffected", "reportTime", "alertCode", "alertName",
		"reason", "alertType", "dateOfVioOrWarn", "startDateTimeOfVio", "maxFdp", "schedFdp", "completedFDP",
		"projCompletedFdpForCrewInAir", "remFdp", "cumFdpIn168", "remCumFdpIn168", "cumFdpIn672", "remCumFdpIn672",
		"maxFltTime", "shedFltTime", "remFltTime", "maxFltTimeIn672", "cumFltTimeIn672", "remFltTimeIn672",
		"maxFltTimeInOneYr", "cumFltTimeInOneYr", "remFltTimeInOneYr", "reqRest", "totalRest",
		"_30HourRestIn168StartDtm", "extension", "extnInMins", "restrictionCode", "expiringQual", "cumStartDtm",
		"cumEndDtm", "cumStartFltNum", "cumStartFltDate", "cumStartFltDepStn", "cumStartFltDep", "cumStartFltArrStn",
		"cumStartFltArrTime", "cumEndFltNum", "cumEndFltDate", "cumEndFltDepStn", "cumEndFltDep", "cumEndFltArrStn",
		"cumEndFltArrTime", "ccoDtm", "ccoTmLeft", "lastFltNum", "lastFltDate", "lastFltDepStn", "lastFltDep",
		"lastFltArrStn", "lastFltArr", "lfMaxDepTime", "lfMaxArrTime", "lfFdp", "reqRestLf", "schdRestLf",
		"outButNotOff" })
public class CrewAlertDTO implements CommonConstants {
	private static final Logger LOGGER = LogManager.getLogger(CrewAlertDTO.class);
	@DynamoDBAttribute(attributeName = "PayrollNumber")
	private String payrollNumber;
	@DynamoDBAttribute(attributeName = "TripDate")
	private String tripDate;
	@DynamoDBAttribute(attributeName = "WorkCode")
	private String workCode;
	@DynamoDBAttribute(attributeName = "FlightNum")
	private String flightNum;
	@DynamoDBAttribute(attributeName = "DepCity")
	private String depCity;
	@DynamoDBAttribute(attributeName = "ArrCity")
	private String arrCity;
	@DynamoDBAttribute(attributeName = "CrewNumber")
	private String crewNumber;
	@DynamoDBAttribute(attributeName = "Seat")
	private String seat;
	@DynamoDBAttribute(attributeName = "Base")
	private String base;
	@DynamoDBAttribute(attributeName = "TripCode")
	private String tripCode;
	@DynamoDBAttribute(attributeName = "TripUniKey")
	private String tripUniKey;
	@DynamoDBAttribute(attributeName = "Segment")
	private String segment;
	@DynamoDBAttribute(attributeName = "TotalFltsAffected")
	private String totalFltsAffected;
	@DynamoDBAttribute(attributeName = "ReportTime")
	private String reportTime;
	@DynamoDBAttribute(attributeName = "AlertCode")
	private String alertCode;
	@DynamoDBAttribute(attributeName = "AlertName")
	private String alertName;
	@DynamoDBAttribute(attributeName = "Reason")
	private String reason;
	@DynamoDBAttribute(attributeName = "AlertType")
	private String alertType;
	@DynamoDBAttribute(attributeName = "DateOfVioOrWarn")
	private String dateOfVioOrWarn;
	@DynamoDBAttribute(attributeName = "StartDateTimeOfVio")
	private String startDateTimeOfVio;
	@DynamoDBAttribute(attributeName = "MaxFdp")
	private String maxFdp;
	@DynamoDBAttribute(attributeName = "SchedFdp")
	private String schedFdp;
	@DynamoDBAttribute(attributeName = "CompletedFDP")
	private String completedFDP;
	@DynamoDBAttribute(attributeName = "ProjCompletedFdpForCrewInAir")
	private String projCompletedFdpForCrewInAir;
	@DynamoDBAttribute(attributeName = "RemFdp")
	private String remFdp;
	@DynamoDBAttribute(attributeName = "CumFdpIn168")
	private String cumFdpIn168;
	@DynamoDBAttribute(attributeName = "RemCumFdpIn168")
	private String remCumFdpIn168;
	@DynamoDBAttribute(attributeName = "CumFdpIn672")
	private String cumFdpIn672;
	@DynamoDBAttribute(attributeName = "RemCumFdpIn672")
	private String remCumFdpIn672;
	@DynamoDBAttribute(attributeName = "MaxFltTime")
	private String maxFltTime;
	@DynamoDBAttribute(attributeName = "ShedFltTime")
	private String shedFltTime;
	@DynamoDBAttribute(attributeName = "RemFltTime")
	private String remFltTime;
	@DynamoDBAttribute(attributeName = "MaxFltTimeIn672")
	private String maxFltTimeIn672;
	@DynamoDBAttribute(attributeName = "CumFltTimeIn672")
	private String cumFltTimeIn672;
	@DynamoDBAttribute(attributeName = "RemFltTimeIn672")
	private String remFltTimeIn672;
	@DynamoDBAttribute(attributeName = "MaxFltTimeInOneYr")
	private String maxFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "CumFltTimeInOneYr")
	private String cumFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "RemFltTimeInOneYr")
	private String remFltTimeInOneYr;
	@DynamoDBAttribute(attributeName = "ReqRest")
	private String reqRest;
	@DynamoDBAttribute(attributeName = "TotalRest")
	private String totalRest;
	@DynamoDBAttribute(attributeName = "_30HourRestIn168StartDtm")
	private String _30HourRestIn168StartDtm;
	@DynamoDBAttribute(attributeName = "Extension")
	private String extension;
	@DynamoDBAttribute(attributeName = "ExtnInMins")
	private String extnInMins;
	@DynamoDBAttribute(attributeName = "RestrictionCode")
	private String restrictionCode;
	@DynamoDBAttribute(attributeName = "ExpiringQual")
	private String expiringQual;
	@DynamoDBAttribute(attributeName = "CumStartDtm")
	private String cumStartDtm;
	@DynamoDBAttribute(attributeName = "CumEndDtm")
	private String cumEndDtm;
	@DynamoDBAttribute(attributeName = "CumStartFltNum")
	private String cumStartFltNum;
	@DynamoDBAttribute(attributeName = "CumStartFltDate")
	private String cumStartFltDate;
	@DynamoDBAttribute(attributeName = "CumStartFltDepStn")
	private String cumStartFltDepStn;
	@DynamoDBAttribute(attributeName = "CumStartFltDep")
	private String cumStartFltDep;
	@DynamoDBAttribute(attributeName = "CumStartFltArrStn")
	private String cumStartFltArrStn;
	@DynamoDBAttribute(attributeName = "CumStartFltArrTime")
	private String cumStartFltArrTime;
	@DynamoDBAttribute(attributeName = "CumEndFltNum")
	private String cumEndFltNum;
	@DynamoDBAttribute(attributeName = "CumEndFltDate")
	private String cumEndFltDate;
	@DynamoDBAttribute(attributeName = "CumEndFltDepStn")
	private String cumEndFltDepStn;
	@DynamoDBAttribute(attributeName = "CumEndFltDep")
	private String cumEndFltDep;
	@DynamoDBAttribute(attributeName = "CumEndFltArrStn")
	private String cumEndFltArrStn;
	@DynamoDBAttribute(attributeName = "CumEndFltArrTime")
	private String cumEndFltArrTime;
	@DynamoDBAttribute(attributeName = "CcoDtm")
	private String ccoDtm;
	@DynamoDBAttribute(attributeName = "CcoTmLeft")
	private String ccoTmLeft;
	@DynamoDBAttribute(attributeName = "LastFltNum")
	private String lastFltNum;
	@DynamoDBAttribute(attributeName = "LastFltDate")
	private String lastFltDate;
	@DynamoDBAttribute(attributeName = "LastFltDepStn")
	private String lastFltDepStn;
	@DynamoDBAttribute(attributeName = "LastFltDep")
	private String lastFltDep;
	@DynamoDBAttribute(attributeName = "LastFltArrStn")
	private String lastFltArrStn;
	@DynamoDBAttribute(attributeName = "LastFltArr")
	private String lastFltArr;
	@DynamoDBAttribute(attributeName = "LfMaxDepTime")
	private String lfMaxDepTime;
	@DynamoDBAttribute(attributeName = "LfMaxArrTime")
	private String lfMaxArrTime;
	@DynamoDBAttribute(attributeName = "LfFdp")
	private String lfFdp;
	@DynamoDBAttribute(attributeName = "ReqRestLf")
	private String reqRestLf;
	@DynamoDBAttribute(attributeName = "SchdRestLf")
	private String schdRestLf;
	@DynamoDBAttribute(attributeName = "OutButNotOff")
	private String outButNotOff;
	@DynamoDBHashKey
	@DynamoDBAttribute(attributeName = "BusinessKey")
	private String businessKey;
	@DynamoDBAttribute(attributeName = "FileName")
	private String fileName;
	@DynamoDBAttribute(attributeName = "CreatedDateTime")
	private String createdDateTime;

	public CrewAlertDTO(@JsonProperty("payrollNumber") String payrollNumber, @JsonProperty("tripDate") String tripDate,
			@JsonProperty("workCode") String workCode, @JsonProperty("flightNum") String flightNum,
			@JsonProperty("depCity") String depCity, @JsonProperty("arrCity") String arrCity,
			@JsonProperty("crewNumber") String crewNumber, @JsonProperty("seat") String seat,
			@JsonProperty("base") String base, @JsonProperty("tripCode") String tripCode,
			@JsonProperty("tripUniKey") String tripUniKey, @JsonProperty("segment") String segment,
			@JsonProperty("totalFltsAffected") String totalFltsAffected, @JsonProperty("reportTime") String reportTime,
			@JsonProperty("alertCode") String alertCode, @JsonProperty("alertName") String alertName,
			@JsonProperty("reason") String reason, @JsonProperty("alertType") String alertType,
			@JsonProperty("dateOfVioOrWarn") String dateOfVioOrWarn,
			@JsonProperty("startDateTimeOfVio") String startDateTimeOfVio, @JsonProperty("maxFdp") String maxFdp,
			@JsonProperty("schedFdp") String schedFdp, @JsonProperty("completedFDP") String completedFDP,
			@JsonProperty("projCompletedFdpForCrewInAir") String projCompletedFdpForCrewInAir,
			@JsonProperty("remFdp") String remFdp, @JsonProperty("cumFdpIn168") String cumFdpIn168,
			@JsonProperty("remCumFdpIn168") String remCumFdpIn168, @JsonProperty("cumFdpIn672") String cumFdpIn672,
			@JsonProperty("remCumFdpIn672") String remCumFdpIn672, @JsonProperty("maxFltTime") String maxFltTime,
			@JsonProperty("shedFltTime") String shedFltTime, @JsonProperty("remFltTime") String remFltTime,
			@JsonProperty("maxFltTimeIn672") String maxFltTimeIn672,
			@JsonProperty("cumFltTimeIn672") String cumFltTimeIn672,
			@JsonProperty("remFltTimeIn672") String remFltTimeIn672,
			@JsonProperty("maxFltTimeInOneYr") String maxFltTimeInOneYr,
			@JsonProperty("cumFltTimeInOneYr") String cumFltTimeInOneYr,
			@JsonProperty("remFltTimeInOneYr") String remFltTimeInOneYr, @JsonProperty("reqRest") String reqRest,
			@JsonProperty("totalRest") String totalRest,
			@JsonProperty("_30HourRestIn168StartDtm") String _30HourRestIn168StartDtm,
			@JsonProperty("extension") String extension, @JsonProperty("extnInMins") String extnInMins,
			@JsonProperty("restrictionCode") String restrictionCode, @JsonProperty("expiringQual") String expiringQual,
			@JsonProperty("cumStartDtm") String cumStartDtm, @JsonProperty("cumEndDtm") String cumEndDtm,
			@JsonProperty("cumStartFltNum") String cumStartFltNum,
			@JsonProperty("cumStartFltDate") String cumStartFltDate,
			@JsonProperty("cumStartFltDepStn") String cumStartFltDepStn,
			@JsonProperty("cumStartFltDep") String cumStartFltDep,
			@JsonProperty("cumStartFltArrStn") String cumStartFltArrStn,
			@JsonProperty("cumStartFltArrTime") String cumStartFltArrTime,
			@JsonProperty("cumEndFltNum") String cumEndFltNum, @JsonProperty("cumEndFltDate") String cumEndFltDate,
			@JsonProperty("cumEndFltDepStn") String cumEndFltDepStn, @JsonProperty("cumEndFltDep") String cumEndFltDep,
			@JsonProperty("cumEndFltArrStn") String cumEndFltArrStn,
			@JsonProperty("cumEndFltArrTime") String cumEndFltArrTime, @JsonProperty("ccoDtm") String ccoDtm,
			@JsonProperty("ccoTmLeft") String ccoTmLeft, @JsonProperty("lastFltNum") String lastFltNum,
			@JsonProperty("lastFltDate") String lastFltDate, @JsonProperty("lastFltDepStn") String lastFltDepStn,
			@JsonProperty("lastFltDep") String lastFltDep, @JsonProperty("lastFltArrStn") String lastFltArrStn,
			@JsonProperty("lastFltArr") String lastFltArr, @JsonProperty("lfMaxDepTime") String lfMaxDepTime,
			@JsonProperty("lfMaxArrTime") String lfMaxArrTime, @JsonProperty("lfFdp") String lfFdp,
			@JsonProperty("reqRestLf") String reqRestLf, @JsonProperty("schdRestLf") String schdRestLf,
			@JsonProperty("outButNotOff") String outButNotOff) {
		super();
		this.payrollNumber = payrollNumber;
		this.tripDate = tripDate;
		this.workCode = workCode;
		this.flightNum = flightNum;
		this.depCity = depCity;
		this.arrCity = arrCity;
		this.crewNumber = crewNumber;
		this.seat = seat;
		this.base = base;
		this.tripCode = tripCode;
		this.tripUniKey = tripUniKey;
		this.segment = segment;
		this.totalFltsAffected = totalFltsAffected;
		this.reportTime = reportTime;
		this.alertCode = alertCode;
		this.alertName = alertName;
		this.reason = reason;
		this.alertType = alertType;
		this.dateOfVioOrWarn = dateOfVioOrWarn;
		this.startDateTimeOfVio = startDateTimeOfVio;
		this.maxFdp = maxFdp;
		this.schedFdp = schedFdp;
		this.completedFDP = completedFDP;
		this.projCompletedFdpForCrewInAir = projCompletedFdpForCrewInAir;
		this.remFdp = remFdp;
		this.cumFdpIn168 = cumFdpIn168;
		this.remCumFdpIn168 = remCumFdpIn168;
		this.cumFdpIn672 = cumFdpIn672;
		this.remCumFdpIn672 = remCumFdpIn672;
		this.maxFltTime = maxFltTime;
		this.shedFltTime = shedFltTime;
		this.remFltTime = remFltTime;
		this.maxFltTimeIn672 = maxFltTimeIn672;
		this.cumFltTimeIn672 = cumFltTimeIn672;
		this.remFltTimeIn672 = remFltTimeIn672;
		this.maxFltTimeInOneYr = maxFltTimeInOneYr;
		this.cumFltTimeInOneYr = cumFltTimeInOneYr;
		this.remFltTimeInOneYr = remFltTimeInOneYr;
		this.reqRest = reqRest;
		this.totalRest = totalRest;
		this._30HourRestIn168StartDtm = _30HourRestIn168StartDtm;
		this.extension = extension;
		this.extnInMins = extnInMins;
		this.restrictionCode = restrictionCode;
		this.expiringQual = expiringQual;
		this.cumStartDtm = cumStartDtm;
		this.cumEndDtm = cumEndDtm;
		this.cumStartFltNum = cumStartFltNum;
		this.cumStartFltDate = cumStartFltDate;
		this.cumStartFltDepStn = cumStartFltDepStn;
		this.cumStartFltDep = cumStartFltDep;
		this.cumStartFltArrStn = cumStartFltArrStn;
		this.cumStartFltArrTime = cumStartFltArrTime;
		this.cumEndFltNum = cumEndFltNum;
		this.cumEndFltDate = cumEndFltDate;
		this.cumEndFltDepStn = cumEndFltDepStn;
		this.cumEndFltDep = cumEndFltDep;
		this.cumEndFltArrStn = cumEndFltArrStn;
		this.cumEndFltArrTime = cumEndFltArrTime;
		this.ccoDtm = ccoDtm;
		this.ccoTmLeft = ccoTmLeft;
		this.lastFltNum = lastFltNum;
		this.lastFltDate = lastFltDate;
		this.lastFltDepStn = lastFltDepStn;
		this.lastFltDep = lastFltDep;
		this.lastFltArrStn = lastFltArrStn;
		this.lastFltArr = lastFltArr;
		this.lfMaxDepTime = lfMaxDepTime;
		this.lfMaxArrTime = lfMaxArrTime;
		this.lfFdp = lfFdp;
		this.reqRestLf = reqRestLf;
		this.schdRestLf = schdRestLf;
		this.outButNotOff = outButNotOff;
		this.businessKey = "CrewAlert::" + crewNumber + "::" + flightNum + "::" + alertCode + "::" + alertType + "::"
				+ startDateTimeOfVio;
		this.createdDateTime = CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT);
		validateBusinessKey(flightNum, crewNumber, alertCode, alertType, startDateTimeOfVio);

	}

	private void validateBusinessKey(String flightNum, String crewNumber, String alertCode, String alertType,
			String startDateTimeOfVio) {
		Pattern pattern = null;
		Matcher matcher = null;
		{
			String numRgx = "^[0-9]{4}$";
			pattern = Pattern.compile(numRgx);
			matcher = pattern.matcher(crewNumber);
			if (matcher.matches()) {
				LOGGER.info("CrewNumber should be 4 digits::" + businessKey);
			}
		}
		{
			String numRgx = "^[0-9]{4}$";
			pattern = Pattern.compile(numRgx);
			matcher = pattern.matcher(flightNum);
			if (matcher.matches()) {
				LOGGER.info("FlightNumber should be 4 digits::" + businessKey);
			}
		}
		if ("".equals(alertCode)) {
			LOGGER.info("AlertCode should not be empty::" + businessKey);
		}
		{
			String charRgx = "^[A-Z]{1}$";
			pattern = Pattern.compile(charRgx);
			matcher = pattern.matcher(alertType);
			if (matcher.matches()) {
				LOGGER.info("AlertType should be single alphabet::" + businessKey);
			}
		}
		{
			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy'T'HH:mm");
				LocalDateTime.parse(startDateTimeOfVio, formatter);
			} catch (DateTimeParseException e) {
				LOGGER.info("Invalid ViolationDateTime format::" + businessKey);
			}
		}

	}

}

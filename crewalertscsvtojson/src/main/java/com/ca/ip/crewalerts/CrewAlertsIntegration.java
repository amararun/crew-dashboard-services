package com.ca.ip.crewalerts;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.AbstractApplicationContext;

@SpringBootApplication

public class CrewAlertsIntegration implements CommandLineRunner {
	static AbstractApplicationContext context;
	// private static final Logger LOGGER =
	// LogManager.getLogger(CrewAlertsIntegration.class);

	public static void main(String[] args) {
		System.out.println("CrewAlerts-IP app has been started listening to FTP directory");
		context = (AbstractApplicationContext) SpringApplication.run(CrewAlertsIntegration.class, args);
		context.start();
	}

	@Override
	public void run(String... args) throws Exception {

	}
}

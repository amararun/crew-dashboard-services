package com.ca.ip.crewalerts.service;

import static com.ca.ip.crewalerts.util.CommonUtil.getExceptionStackTrace;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ca.ip.crewalerts.dao.CrewAlertsDAO;
import com.ca.ip.crewalerts.model.CrewAlertDTO;
import com.ca.ip.crewalerts.util.CommonConstants;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;

@Service
public class CrewAlertsFileHandler implements CommonConstants {
	private static final Logger LOGGER = LogManager.getLogger(FileHandler.class);
	@Autowired
	CrewAlertsDAO crewAlertsDAO;

	public File handleFile(File csvFile) {
		if (csvFile.renameTo(csvFile)) {
			// List<CrewAlertsDetail> crewAlertsDetailList = new
			// ArrayList<CrewAlertsDetail>();
			List<CrewAlertDTO> crewAlertsDTOs = new ArrayList<CrewAlertDTO>();
			LOGGER.info("Inside handleFile method in class CrewAlertsFileHandler");
			crewAlertsDTOs = convertCsvToObject(csvFile, crewAlertsDTOs);
			if (!crewAlertsDTOs.isEmpty()) {
				// crewAlertsDTOList = createDynamoDBObject(crewAlertsDetailList,
				// crewAlertsDTOList, csvFile.getName());
				crewAlertsDAO.saveCrewAlertsDTO(crewAlertsDTOs);
			}
			return csvFile;
		}
		return null;
	}

	private List<CrewAlertDTO> convertCsvToObject(File csvFile, List<CrewAlertDTO> crewAlertsDTOs) {
		MappingIterator<CrewAlertDTO> iterator;
		try {
			iterator = new CsvMapper().readerWithTypedSchemaFor(CrewAlertDTO.class).readValues(csvFile);
			crewAlertsDTOs = iterator.readAll();
			crewAlertsDTOs.forEach((crewAlertsDTO) -> {
				crewAlertsDTO.setFileName(csvFile.getName());
			});
		} catch (IOException e) {
			LOGGER.error(getExceptionStackTrace(e));
		}
		return crewAlertsDTOs;
	}

	/*
	 * private List<CrewAlertsDTO> createDynamoDBObject(List<CrewAlertsDetail>
	 * crewAlertsDetailList, List<CrewAlertsDTO> crewAlertsDTOList, String
	 * csvFileName) { crewAlertsDetailList.forEach(crewAlerts -> { CrewAlertsDTO
	 * crewAlertsDTO = new CrewAlertsDTO();
	 * crewAlertsDTO.setCrewID(crewAlerts.getCrewID());
	 * crewAlertsDTO.setPayRollNumber(crewAlerts.getPayRollNumber());
	 * crewAlertsDTO.setFileName(csvFileName);
	 * crewAlertsDTO.setCreatedDate(convertDateToString(new Date(),
	 * ISO_DATE_TIME_FORMAT)); crewAlertsDTO.setCrewAlertsDetail(crewAlerts);
	 * crewAlertsDTOList.add(crewAlertsDTO); }); return crewAlertsDTOList; }
	 */
}

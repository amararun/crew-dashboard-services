package com.ca.ip.crewalerts.model;

public class CrewAlertsProperties {
	private String tableName;
	private String s3BucketPath;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getS3BucketPath() {
		return s3BucketPath;
	}

	public void setS3BucketPath(String s3BucketPath) {
		this.s3BucketPath = s3BucketPath;
	}

}

package com.ca.ip.crewalerts;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.ChainFileListFilter;
import org.springframework.integration.file.filters.LastModifiedFileListFilter;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;
import org.springframework.messaging.MessageChannel;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ca.ip.crewalerts.service.CrewAlertsFileHandler;

@Configuration
@EnableIntegration
@PropertySource("classpath:application.properties")
public class CrewAlertsIntegrationConfiguration {
	@Value("${sourceFilePath}")
	private String INPUT_DIR;

	@Value("${regexPatternFileListFilter}")
	private String FILE_PATTERN;

	@Value("${aws.access_key_id}")
	private String awsId;

	@Value("${aws.secret_access_key}")
	private String awsKey;

	@Value("${aws.region}")
	private String region;

	@Value("${aws.s3.endpoint}")
	private String s3EndPoint;

	@Value("${s3Bucket}")
	private String s3Bucket;

	@Value("${age}")
	private int age;

	@Autowired
	CrewAlertsFileHandler crewAlertsFileHandler;

	@Bean
	public BasicAWSCredentials awsCredentials() {
		return new BasicAWSCredentials(awsId, awsKey);
	}

	@Bean
	public AWSCredentialsProvider awsCredentialsProvider() {
		return new AWSCredentialsProvider() {

			@Override
			public void refresh() {
			}

			@Override
			public AWSCredentials getCredentials() {
				return awsCredentials();
			}
		};
	}

	@Bean
	public AmazonS3 amazonS3() {

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(s3EndPoint, region))
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials())).build();

		return s3Client;
	}

	@Bean
	public AmazonDynamoDB amazonDynamoDB() {
		return AmazonDynamoDBClientBuilder.standard().withCredentials(awsCredentialsProvider()).withRegion(region)
				.build();
	}

	@Bean
	public DynamoDBMapper dynamoDBMapper() {
		return new DynamoDBMapper(amazonDynamoDB());
	}

	@Bean
	public MessageChannel fileInputChannel() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel fileOutputChannel() {
		return new DirectChannel();
	}

	@Bean
	@InboundChannelAdapter(value = "fileInputChannel", poller = @Poller(fixedDelay = "1000"), autoStartup = "false")
	public MessageSource<File> fileReadingMessageSource() {
		FileReadingMessageSource sourceReader = new FileReadingMessageSource();
		sourceReader.setDirectory(new File(INPUT_DIR));
		// sourceReader.setFilter(new RegexPatternFileListFilter(FILE_PATTERN));
		sourceReader.setFilter(new ChainFileListFilter<File>().addFilter(new LastModifiedFileListFilter(age))
				.addFilter(new RegexPatternFileListFilter(FILE_PATTERN)));
		return sourceReader;
	}

	@Transformer(inputChannel = "fileInputChannel", outputChannel = "fileOutputChannel", autoStartup = "false")
	public File fileHandler() {
		File csvFile = this.fileReadingMessageSource().receive().getPayload();
		crewAlertsFileHandler.handleFile(csvFile);
		// S3MessageHandler s3MessageHandler = new S3MessageHandler(amazonS3(),
		// s3Bucket);

		return csvFile;
	}

	@ServiceActivator(inputChannel = "fileOutputChannel", autoStartup = "false")
	public void uploadCsvFileToS3(File csvFile) {
		if (null != csvFile) {
			amazonS3().putObject(new PutObjectRequest(s3Bucket, csvFile.getName(), csvFile));
			csvFile.delete();
		}

	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}

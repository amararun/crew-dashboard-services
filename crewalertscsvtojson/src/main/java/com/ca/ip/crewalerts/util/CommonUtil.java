package com.ca.ip.crewalerts.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class CommonUtil implements CommonConstants {
	public static String getExceptionStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	public static String convertDateToString(Date date, String dateFormat) {
		String formatedDate = EMPTY_STRING;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		formatedDate = simpleDateFormat.format(date);
		return formatedDate;
	}

	public static long getTTLUnits(String date, String format, int numberOfDays) throws ParseException {
		LocalDateTime dateTime = LocalDateTime.now();
		dateTime.plusDays(numberOfDays);
		return dateTime.atZone(ZoneId.of("America/New_York")).toEpochSecond();
	}

	public static String getCurrentESTTime(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZoneId newYokZoneId = ZoneId.of("America/New_York");
		ZonedDateTime zonedDateTime = dateTime.atZone(newYokZoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(zonedDateTime);
	}
}

package com.ca.ip.crewalerts.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.ca.ip.crewalerts.model.CrewAlertDTO;
import com.ca.ip.crewalerts.util.CommonConstants;

@Service
public class CrewAlertsDAO implements CommonConstants {
	private static final Logger LOGGER = LogManager.getLogger(CrewAlertsDAO.class);
	@Autowired
	DynamoDBMapper dynamoDBMapper;
	@Autowired
	AmazonDynamoDB amazonDynamoDB;
	@Value("${tableName}")
	private String tableName;
	@Value("${historyTableName}")
	private String historyTableName;
	@Value("${ttlDays}")
	private String ttlDays;

	public void saveCrewAlertsDTO(List<CrewAlertDTO> crewAlertsDTOs) {
		LOGGER.info("Inside saveCrewAlertsDTO method in class CrewAlertsDAO");
		emptyCrewAlertsTable();

		crewAlertsDTOs.stream().forEach(crewAlertDTO -> {
			dynamoDBMapper.save(crewAlertDTO,
					DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName).config());
		});

		/*
		 * crewAlertsDTOs.stream().forEach(alertsDTO -> { CrewAlertHistoryDTO
		 * alertHistoryDTO = new CrewAlertHistoryDTO();
		 * BeanUtils.copyProperties(alertsDTO, alertHistoryDTO); alertHistoryDTO.setId(
		 * "AlertHistory::" + alertHistoryDTO.getCrewNumber() + "::" +
		 * alertHistoryDTO.getDateOfVioOrWarn()); try {
		 * alertHistoryDTO.setTtl(CommonUtil.getTTLUnits(alertHistoryDTO.
		 * getDateOfVioOrWarn(), "yyyy-MM-dd", Integer.valueOf(ttlDays)));
		 * alertHistoryDTO.setCreatedDtTmHistry(CommonUtil.getCurrentESTTime(
		 * ISO_DATE_TIME_FORMAT)); } catch (NumberFormatException e) {
		 * CommonUtil.getExceptionStackTrace(e); } catch (ParseException e) {
		 * CommonUtil.getExceptionStackTrace(e); } dynamoDBMapper.save(alertHistoryDTO,
		 * DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(
		 * historyTableName).config()); });
		 */
		LOGGER.info("Data saved successfully in dynamoDB");

	}

	private void emptyCrewAlertsTable() {
		LOGGER.info("Inside emptyCrewAlertsTable method in class CrewAlertsDAO");
		List<CrewAlertDTO> crewAlertsDTOs = dynamoDBMapper.parallelScan(CrewAlertDTO.class,
				new DynamoDBScanExpression(), 50,
				DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName).config());
		LOGGER.info("Total CrewAlerts records to be deleted - " + crewAlertsDTOs.size());

		crewAlertsDTOs.stream().forEach(crewAlertDTO -> {
			dynamoDBMapper.delete(crewAlertDTO,
					DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName).config());
		});

		LOGGER.info("CrewAlerts records deleted successfully");
	}

}

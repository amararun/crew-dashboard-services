package com.commutair.ip.crewvacationapi;

public interface CommonConstants {
	String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	String ISO_DATE_FORMAT = "yyyy-MM-dd";

	String EMPTY_STRING = "";
	String DOUBLE_COLON = "::";

	String CREW_CALENDAR_KEY = "CrewClndr";
	String CREW_VACATION_KEY = "CrewVacation";

	String DELETE = "D";

	String BUSINESS_KEY = "BusinessKey";
	String CREW_NUMBER = "CrewNumber";
	String CREW_ID = "CrewID";
	String CATEGORY = "Category";
	String VACATION_DATE = "VacationDate";
	String CHANGE_TYPE = "ChangeType";
	String FROM_DATE = "FromDate";
	String TO_DATE = "ToDate";
	String LAST_UPDATED_DT_TM = "LastUpdatedDtTm";
	String TTL = "TTL";

	String[] VACATION_CATEGORY = { "K", "VA", "WC", "MI", "M1", "M2", "M3", "M4", "FA", "F1", "F2", "F3", "F4", "O" };
}

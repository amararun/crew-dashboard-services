/**
 * 
 */
package com.commutair.ip.crewvacationapi;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

/**
 * @author Arun.Kumar
 *
 */
@Data
@DynamoDBTable(tableName = "DEV.CREW_CALENDAR")
public class CrewCalendar {

	@DynamoDBHashKey
	private String businessKey;
	private List<String> crewActivities;
	private long ttl;

}

package com.commutair.ip.crewvacationapi;

import static com.commutair.ip.crewvacationapi.CommonUtil.findBidMonth;
import static com.commutair.ip.crewvacationapi.CommonUtil.getISODateTimeFormat;
import static com.commutair.ip.crewvacationapi.CommonUtil.handleException;
import static com.commutair.ip.crewvacationapi.CommonUtil.setResponse;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CrewVacationHandler implements RequestHandler<Request, Response>, CommonConstants {
	private DynamoDB dynamoDB;
	private LambdaLogger logger;
	private AmazonDynamoDB client;
	private DynamoDBMapper dynamoDBMapper;

	public Response handleRequest(Request request, Context context) {
		Response response = null;
		// Initializing logger
		logger = context.getLogger();
		logger.log("message request - " + request.toString());
		try {
			// Retrieving environment variables
			String vacationTableName = System.getenv("vacationTableName");
			String calendarTableName = System.getenv("calendarTableName");
			String region = System.getenv("region");
			String endPoint = System.getenv("endPoint");
			String ttlDays = System.getenv("ttlDays");

			// Initializing connection to DynamoDB
			this.initDynamoDbClient(region, endPoint);

			// Converting Json to object
			ObjectMapper mapper = new ObjectMapper();
			CrewVacationDTO crewVacationDTO = mapper.readValue(request.getBody(), CrewVacationDTO.class);

			List<String> category = Arrays.asList(VACATION_CATEGORY);// Loading vacation category

			if (category.contains(crewVacationDTO.getCategory())) {// Validating vacation category

				// Setting time to live
				long ttl = CommonUtil.getTTLUnits(crewVacationDTO.getVacDate(), ISO_DATE_FORMAT,
						Integer.valueOf(ttlDays));

				// Finding bid month to get crew calendar object
				String bidMonth = findBidMonth(crewVacationDTO.getVacDate());
				logger.log("Bid Month = " + bidMonth);

				// Creating calendar and vacation business keys to insert/remove records
				String calendarBusinessKey = CREW_CALENDAR_KEY + DOUBLE_COLON + crewVacationDTO.getCrewNumber()
						+ DOUBLE_COLON + bidMonth;
				String vacBusinessKey = CREW_VACATION_KEY + DOUBLE_COLON + crewVacationDTO.getCrewNumber()
						+ DOUBLE_COLON + crewVacationDTO.getVacDate() + DOUBLE_COLON + crewVacationDTO.getCategory();

				// Fetching crew calendar object
				CrewCalendar crewCalendar = this.constructCalendarDetails(calendarBusinessKey, ttl);

				// DELETE Operation
				if (crewVacationDTO.getChangeType().equalsIgnoreCase(DELETE)) {
					try {
						deleteData(vacBusinessKey, vacationTableName);
						crewCalendar.getCrewActivities().remove(vacBusinessKey);
						persistData(crewCalendar, calendarTableName);
						logger.log("Deleted data - " + crewVacationDTO);
						response = setResponse("{\"message\":\"Data deleted successfully\"}", 200);
					} catch (Exception e) {
						response = handleException(e, "{\"message\":\"Data failed during delete operation\"}");
					}
				} // INSERT Operation
				else {
					try {
						crewVacationDTO.setLastUpdatedDtTm(CommonUtil.getCurrentESTTime(ISO_DATE_TIME_FORMAT));
						crewVacationDTO.setTtl(ttl);
						persistData(crewVacationDTO, vacBusinessKey, vacationTableName);
						if (!crewCalendar.getCrewActivities().contains(vacBusinessKey)) {
							crewCalendar.getCrewActivities().add(vacBusinessKey);
							persistData(crewCalendar, calendarTableName);
							logger.log("Persisted data - " + crewVacationDTO);
						}
						response = setResponse("{\"message\":\"Data saved successfully\"}", 200);
					} catch (Exception e) {
						response = handleException(e, "{\"message\":\"Data failed during insert operation\"}");
					}
				}
			} else {
				response = setResponse("{\"message\":\"Vacation Category doesnot match\"}", 420);
			}
		}

		catch (JsonParseException e) {
			response = handleException(e, "{\"message\":\"JsonParseException- Invalid Json\"}");
		} catch (JsonMappingException e) {
			response = handleException(e, "{\"message\":\"JsonMappingException-Invalid Json\"}");
		} catch (IOException e) {
			response = handleException(e, "{\"message\":\"IOException-Error occured while reading Json\"}");
		} catch (ParseException e) {
			response = handleException(e, "{\"message\":\"Exception occured\"}");
		}
		return response;

	}

	private PutItemOutcome persistData(CrewVacationDTO crewVacationDTO, String businessKey, String tableName)
			throws ConditionalCheckFailedException, ParseException {

		logger.log("Entering persistData function " + businessKey);
		return this.dynamoDB.getTable(tableName).putItem(new PutItemSpec().withItem(new Item()
				.withString(BUSINESS_KEY, businessKey).withString(CREW_NUMBER, crewVacationDTO.getCrewNumber())
				.withString(CREW_ID, crewVacationDTO.getCrewID()).withString(CATEGORY, crewVacationDTO.getCategory())
				.withString(VACATION_DATE, crewVacationDTO.getVacDate())
				.withString(CHANGE_TYPE, crewVacationDTO.getChangeType())
				.withString(FROM_DATE, getISODateTimeFormat(crewVacationDTO.getVacDate(), true))
				.withString(TO_DATE, getISODateTimeFormat(crewVacationDTO.getVacDate(), false))
				.withString(LAST_UPDATED_DT_TM, crewVacationDTO.getLastUpdatedDtTm())
				.withLong(TTL, crewVacationDTO.getTtl())));

	}

	private DeleteItemOutcome deleteData(String businessKey, String tableName) throws ConditionalCheckFailedException {

		logger.log("Entering deleteData function " + businessKey);
		return this.dynamoDB.getTable(tableName)
				.deleteItem(new DeleteItemSpec().withPrimaryKey(new PrimaryKey(BUSINESS_KEY, businessKey)));

	}

	private CrewCalendar constructCalendarDetails(String businessKey, long ttl) {

		logger.log("Entering constructCalendarDetails " + businessKey);
		List<String> crewActivities = new ArrayList<String>();
		CrewCalendar crewCalendar = dynamoDBMapper.load(CrewCalendar.class, businessKey);
		if (crewCalendar == null) {
			crewCalendar = new CrewCalendar();
			crewCalendar.setBusinessKey(businessKey);
			crewCalendar.setCrewActivities(crewActivities);
			crewCalendar.setTtl(ttl);
		}
		logger.log("Exit constructCalendarDetails");
		return crewCalendar;
	}

	private void persistData(Object objectDTO, String tableName) throws ConditionalCheckFailedException {
		logger.log("Entering persistData function");
		dynamoDBMapper.save(objectDTO, new TableNameOverride(tableName).config());
	}

	private void initDynamoDbClient(String region, String endPoint) {

		if (this.client == null) {
			logger.log("Creating client object");
			client = AmazonDynamoDBClientBuilder.standard()
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, region)).build();
		}
		if (this.dynamoDB == null) {
			logger.log("Creating dynamodb object");
			this.dynamoDB = new DynamoDB(client);
		}
		if (this.dynamoDBMapper == null) {
			logger.log("Creating dynamodb mapper");
			this.dynamoDBMapper = new DynamoDBMapper(client);
		}
	}
}

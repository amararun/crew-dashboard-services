/**
 * 
 */
package com.commutair.ip.crewvacationapi;

import lombok.Data;

@Data
public class CrewVacationDTO {
	private String crewNumber;
	private String crewID;
	private String category;
	private String vacDate;
	private String changeType;
	private String fromDate;
	private String toDate;
	private String lastUpdatedDtTm;
	private long ttl;
}
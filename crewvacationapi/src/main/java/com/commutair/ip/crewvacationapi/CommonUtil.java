package com.commutair.ip.crewvacationapi;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sathish.Ganapathi
 *
 */
public class CommonUtil {
	/**
	 * This method converts current date in required format
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentESTTime(String dateFormat) {
		LocalDateTime dateTime = LocalDateTime.now();
		ZoneId newYokZoneId = ZoneId.of("America/New_York");
		ZonedDateTime zonedDateTime = dateTime.atZone(newYokZoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
		return formatter.format(zonedDateTime);
	}

	/**
	 * This method converts date from one format to another format
	 * 
	 * @param date
	 * @param fromFormat
	 * @param toFormat
	 * @return mintDate
	 * @throws ParseException
	 */
	public static String getCustomizedDate(String dateString, String fromFormat, String toFormat)
			throws ParseException {
		SimpleDateFormat fromFormatter = new SimpleDateFormat(fromFormat);
		Date date = fromFormatter.parse(dateString);
		SimpleDateFormat toFormatter = new SimpleDateFormat(toFormat);
		String formattedDate = toFormatter.format(date);
		return formattedDate;
	}

	/**
	 * This method converts input date string of any format to Date object
	 * 
	 * @param dateString
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Date getDate(String dateString, String format) throws ParseException {
		SimpleDateFormat dataPointFormat = new SimpleDateFormat(format);
		Date date = dataPointFormat.parse(dateString);
		return date;
	}

	/**
	 * This method stack trace of given exception
	 * 
	 * @param e
	 * @return
	 */
	public static String getExceptionStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	/**
	 * This method generates epoch second for given date and offset
	 * 
	 * @param date
	 * @param format
	 * @param numberOfDays
	 * @return
	 * @throws ParseException
	 */
	public static long getTTLUnits(String date, String format, int numberOfDays) throws ParseException {
		LocalDateTime dateTime = LocalDateTime.now();
		dateTime.plusDays(numberOfDays);
		return dateTime.atZone(ZoneId.of("America/New_York")).toEpochSecond();
	}

	public static String findBidMonth(String date) throws ParseException {
		// String tripDate=CommonUtil.getCustomizedDate(tripDate, "MM/dd/yyyy",
		// "yyyy-MM-dd");
		Date tripDt = CommonUtil.getDate(date, "yyyy-MM-dd");
		String year = CommonUtil.getCustomizedDate(date, "yyyy-MM-dd", "yyyy");
		if (tripDt.compareTo(CommonUtil.getDate("01/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("01/30/" + year, "MM/dd/yyyy")) <= 0)
			return "01/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("01/31/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("03/01/" + year, "MM/dd/yyyy")) <= 0)
			return "02/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("03/02/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("03/31/" + year, "MM/dd/yyyy")) <= 0)
			return "03/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("04/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("04/30/" + year, "MM/dd/yyyy")) <= 0)
			return "04/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("05/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("05/31/" + year, "MM/dd/yyyy")) <= 0)
			return "05/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("06/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("06/30/" + year, "MM/dd/yyyy")) <= 0)
			return "06/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("07/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("07/31/" + year, "MM/dd/yyyy")) <= 0)
			return "07/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("08/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("08/31/" + year, "MM/dd/yyyy")) <= 0)
			return "08/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("09/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("09/30/" + year, "MM/dd/yyyy")) <= 0)
			return "09/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("10/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("10/31/" + year, "MM/dd/yyyy")) <= 0)
			return "10/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("11/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("11/30/" + year, "MM/dd/yyyy")) <= 0)
			return "11/" + year;
		if (tripDt.compareTo(CommonUtil.getDate("12/01/" + year, "MM/dd/yyyy")) >= 0
				&& tripDt.compareTo(CommonUtil.getDate("12/31/" + year, "MM/dd/yyyy")) <= 0)
			return "12/" + year;
		return null;
	}

	public static Response handleException(Exception e, String jsonMessage) {
		Response response = null;
		response = setResponse(jsonMessage, 420);
		return response;
	}

	public static Response setResponse(String responseBody, int statusCode) {
		Response response = new Response();
		response.setBase64Encoded(false);
		response.setStatusCode(statusCode);
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("service-name", "crewvacation");
		response.setHeaders(headerMap);
		response.setBody(responseBody);
		return response;
	}

	public static String getISODateTimeFormat(String dateToBeFormatted, boolean dayStart) throws ParseException {
		DateTimeFormatter fromFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse(dateToBeFormatted, fromFormatter);
		DateTimeFormatter toFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		String formattedDate = (dayStart) ? localDate.atStartOfDay().format(toFormatter)
				: localDate.atTime(23, 59, 59).format(toFormatter);
		return formattedDate;
	}

}
